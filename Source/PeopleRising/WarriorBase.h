// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Engine/StreamableManager.h"
#include "AIController.h"
#include "StructsManager.h"
#include "Kismet/KismetMathLibrary.h"
#include "WarriorBase.generated.h"

class ABattleController;
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnEffectStart);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnTurnReady);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_FourParams(FOnDamageRecived, int32, DamageAmount,FVector,TargetLocation,bool,IsDead, bool, ShouldNotifyController);
UCLASS()
class PEOPLERISING_API AWarriorBase : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AWarriorBase();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	

	/* battle properties start*/
	UPROPERTY(BlueprintReadOnly)
	FWarriorData WarriorData;	
	UPROPERTY(BlueprintReadOnly)
	FClassData ClassData;
	UPROPERTY(BlueprintReadOnly)
		FWeaponData Weapon;	
	UPROPERTY(BlueprintReadOnly)
		AWarriorBase* CurrentTarget;
	UPROPERTY(BlueprintReadOnly)
		FWeaponData Armor;
	UPROPERTY(BlueprintReadOnly)
		FWeaponData Accessory;
	UPROPERTY(BlueprintReadOnly)
		FWeaponData Shield;
	UPROPERTY(BlueprintReadOnly)
		TArray<FSkillData>AssignedSkills;
	UPROPERTY(BlueprintReadOnly)
		TArray<FBuffData>CurrentBuffs;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Battle Actions")
		FCharacterState CurrentState;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Battle Actions")
		int32 MoveWeight;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Battle Actions")
		int32 AttackWeight;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Battle Actions")
		int32 CurrentActionWeight;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Battle Actions")
		float RotationSpeed;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Battle Actions")
		int32 RemainingActions;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Battle Actions")
		int32 MaxActions;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Battle Actions")
		int32 MovementRange;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Battle Actions")
		int32 AttackRange;
	/* battle properties end*/

	/* presnetation properties start*/
	UPROPERTY()
		TAssetPtr<USkeletalMesh> WarriorDefaultMesh;
	UPROPERTY(BlueprintReadOnly)
		UStaticMesh* WeaponMesh;
	UPROPERTY(BlueprintReadOnly)
		UArrowComponent * TurnIndicator;

	/* presnetation properties start*/


	/* battle methods start*/
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = AI)
		void RunAITurn();
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Battle Actions")
		void ActionFinished(int32 ActionWeight, bool bShouldResetCamera=true);
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Battle Actions")
		void IncrementActionMeter(int32 Amount);
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Battle Actions")
		void StartTurn();
	UFUNCTION(BlueprintCallable, Category = "Battle Actions")
		void EndTurn();
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Battle Actions")
		void DoAction();
	UFUNCTION( BlueprintCallable, Category = "Battle Actions")
		void AddBuff(const FBuffData Buff);
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Battle Actions")
		//************************************
		// Method:    Move
		// FullName:  AWarriorBase::Move
		// Access:    public 
		// Returns:   void
		// Qualifier:
		// Parameter: FVector Target
		//************************************
		void Move(FVector Target);
	UFUNCTION(BlueprintCallable, Category = "States")
		int32 GetStateModifier(FStatusType Status);
	UFUNCTION(BlueprintCallable, Category = "States")
		bool IsCPU();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Battle Actions")
		void Attack(AWarriorBase* Target);
	//Apply the given amount of damage to the warrior and calls the releveant functions
	// depeneding on the outcome, like destory actor if HP is <=0
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Battle Actions")
		void ApplyDamage(float DamageAmount,bool FinishAction=true);


	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Battle Actions")
		bool bIsTargetInRange(AWarriorBase* Target);
	UFUNCTION(BlueprintCallable, Category = "Battle Actions")
		void LookAt(AWarriorBase* Target);
	UFUNCTION(BlueprintNativeEvent, Category = "Battle Actions")
		void PlayTurnStartIndicator();
	UFUNCTION(BlueprintCallable, Category = "Morph Actions")
		bool IsHPLower(float HPRange);
	UFUNCTION(BlueprintCallable, Category = "Morph Actions")
		bool IsHPHigher(float HPRange);
	UFUNCTION(BlueprintCallable, Category = "Morph Actions")
		bool HasBuffOfType(FStatusType BuffType);
	/* battle methods start*/


	// Called every frame
	//virtual void Tick(float DeltaTime) override;
	
	FStreamableManager * AssetLoader;
	UPROPERTY(BlueprintReadOnly)
	ABattleController * GameController;
	
	UPROPERTY(BlueprintCallable)
	FOnEffectStart EffectStart;
	UPROPERTY(BlueprintCallable)
	FOnDamageRecived DamageEvent;
	UPROPERTY(BlueprintCallable)
	FOnTurnReady OnTurnready;

	//functions
	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = AI)
		FTransform GetCameraTransform();
	

	int32 ActionMeter;
	int32 ActionWeight;
	bool bIsCPU;
	FRotator DesiredRotation;
	bool bShouldTurn;
	float DamageAmount;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Battle Actions")
	FVector2D CurrentLocation;
	
	 
private:
	
	int32 CurrentHP;
	float GetBufModification(FStatusType StatusType);

	
	
};
