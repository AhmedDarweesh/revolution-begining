// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TargetSelectionStartegy.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UTargetSelectionStartegy : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class PEOPLERISING_API ITargetSelectionStartegy
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	
	
};
