// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Startegies/Interfaces/EffectApplyerStartegy.h"
#include "UObject/NoExportTypes.h"
#include "DamageEffectStartegy.generated.h"

/**
 * 
 */
UCLASS()
class PEOPLERISING_API UDamageEffectStartegy : public UObject, public IEffectApplyerStartegy
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "MyCategory")
		void ApplyEffect(AWarriorBase* Initiator, AWarriorBase* Target, FSkillData Skill, bool finishAction);
		virtual void ApplyEffect_Implementation(AWarriorBase* Initiator, AWarriorBase* Target, FSkillData Skill, bool finishAction) override;
	
	
};
