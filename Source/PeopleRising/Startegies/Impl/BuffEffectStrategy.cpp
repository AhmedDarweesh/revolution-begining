// Fill out your copyright notice in the Description page of Project Settings.

#include "BuffEffectStrategy.h"
#include "GameFrameworkObjects/BaseGameInstance.h"

UBuffEffectStrategy::UBuffEffectStrategy()
{
	
}

void UBuffEffectStrategy::ApplyEffect_Implementation(AWarriorBase * Initiator, AWarriorBase * Target, FSkillData Skill,bool FinishAction)
{
	if (WarriorManager == nullptr)
	{
		auto world = GetWorld();
		if (world == nullptr || world->GetGameInstance() == nullptr)
		{
			Initiator->ActionFinished(Skill.ActionWeight,true);
			return;
		}
		auto Gamemanager = Cast<UBaseGameInstance>(world->GetGameInstance());
		WarriorManager = Gamemanager->GetWarriorsManager();
	}
	FString BuffId=Skill.BuffPointer;
	FBuffData SkillBuff = WarriorManager->loadBuff(BuffId);
	if (Target != nullptr)
	{
		Target->AddBuff(SkillBuff);
	}
	if (FinishAction)
	{
	Initiator->ActionFinished(Skill.ActionWeight,true);

	}
}

void UBuffEffectStrategy::SetWarriormanager(UWarriorManager * WarriorManager)
{
	this->WarriorManager = WarriorManager;
}
