// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Startegies/Interfaces/EffectApplyerStartegy.h"
#include "WarriorManager.h"
#include "UObject/NoExportTypes.h"
#include "BuffEffectStrategy.generated.h"

/**
 * 
 */
UCLASS()
class PEOPLERISING_API UBuffEffectStrategy : public UObject, public IEffectApplyerStartegy
{
	GENERATED_BODY()
	
	
public:
	UBuffEffectStrategy();
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "MyCategory")
		void ApplyEffect(AWarriorBase* Initiator, AWarriorBase* Target, FSkillData Skill, bool finishAction);
	virtual void ApplyEffect_Implementation(AWarriorBase* Initiator, AWarriorBase* Target, FSkillData Skill, bool finishAction) override;
	UFUNCTION()
		void SetWarriormanager(UWarriorManager* WarriorManager);
private :
	UPROPERTY()
	UWarriorManager* WarriorManager;
};
