// Fill out your copyright notice in the Description page of Project Settings.

#include "DamageEffectStartegy.h"

void UDamageEffectStartegy::ApplyEffect_Implementation(AWarriorBase * Initiator, AWarriorBase * Target, FSkillData Skill, bool finishAction)
{
	auto SkillType = Skill.SkillDamageType;
	int32 StatusForDamageCalculation = 1;
	switch (SkillType)
	{
	case FDamageType::Physical:
		StatusForDamageCalculation = Initiator->WarriorData.atk;
		break;
	case FDamageType::Magical:
		StatusForDamageCalculation = Initiator->WarriorData.wis;
		break;
	}
	float totalDamage = (Skill.BasePower*StatusForDamageCalculation)*FMath::FRandRange(0.9,1.2);
	
		
			Target->ApplyDamage(totalDamage,finishAction);
			//Initiator->ActionFinished(Skill.ActionWeight,false);
		
	
	

}
