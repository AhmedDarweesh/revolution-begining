// Fill out your copyright notice in the Description page of Project Settings.

#include "EffectApplyerObjectFactory.h"
#include "GameFrameworkObjects/BaseGameInstance.h"

UEffectApplyerObjectFactory::UEffectApplyerObjectFactory()
{


}

UObject * UEffectApplyerObjectFactory::GetEffectStartegyObject(FSkillData Skill,UWorld * World)
{
	if (DamageEffectObject == nullptr || BuffEffectObject==nullptr)
	{
		InitObjects(World);
	}
	switch (Skill.SkillType)
	{
	case FSkillType::Damage:
		return DamageEffectObject;
	case FSkillType::Buff:
	case FSkillType::Debuff:
		return BuffEffectObject;
	default:
		break;
	}
	return nullptr;
}

void UEffectApplyerObjectFactory::InitObjects(UWorld * World)
{
	DamageEffectObject = NewObject<UDamageEffectStartegy>();
	BuffEffectObject = NewObject<UBuffEffectStrategy>();
	if (World == nullptr || World->GetGameInstance() == nullptr)
	{
		return;
	}
	auto Gamemanager = Cast<UBaseGameInstance>(World->GetGameInstance());
	auto WarriorManager = Gamemanager->GetWarriorsManager();
	BuffEffectObject->SetWarriormanager(WarriorManager);
}
