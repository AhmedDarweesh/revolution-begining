// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Startegies/Impl/DamageEffectStartegy.h"
#include "Startegies/Impl/BuffEffectStrategy.h"
#include "UObject/NoExportTypes.h"
#include "EffectApplyerObjectFactory.generated.h"

/**
 * 
 */
UCLASS()
class PEOPLERISING_API UEffectApplyerObjectFactory : public UObject
{
	GENERATED_BODY()
	
public:
	UEffectApplyerObjectFactory();
	UFUNCTION()
	UObject* GetEffectStartegyObject(FSkillData Skill, UWorld* World);

private:
	UPROPERTY()
	UDamageEffectStartegy* DamageEffectObject;
	UPROPERTY()
		UBuffEffectStrategy* BuffEffectObject;
	UFUNCTION()
	void InitObjects(UWorld * World);
	
	
};
