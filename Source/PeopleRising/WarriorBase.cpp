// Fill out your copyright notice in the Description page of Project Settings.

#include "WarriorBase.h"
#include "Elementalists.h"
#include "BattleController.h"
#include "Components/ArrowComponent.h"
#include "Components/CapsuleComponent.h"


// Sets default values
AWarriorBase::AWarriorBase()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	TurnIndicator = CreateDefaultSubobject<UArrowComponent>("TurnIndicator");
	TurnIndicator->AddLocalRotation(FQuat(0, -90, 0, 0));
	TurnIndicator->ArrowSize = 2;
	TurnIndicator->ArrowColor = FColor(0, 255, 0, 200);
	TurnIndicator->SetVisibility(false);
	TurnIndicator->bHiddenInGame = false;
	TurnIndicator->SetupAttachment(GetCapsuleComponent());

}

// Called when the game starts or when spawned
void AWarriorBase::BeginPlay()
{
	//GameController = Cast<ABattleController>(UGameplayStatics::GetPlayerController(GetWorld(), 0));
	Super::BeginPlay();
	CurrentHP = WarriorData.MaxHP;

	MaxActions = FMath::Max(2, WarriorData.MaxActions);
	MovementRange = FMath::Max(3, WarriorData.MovementRange);
	AttackRange = FMath::Max(1, Weapon.AttackRange);
	ActionMeter = WarriorData.spd;
	UE_LOG(LogTemp, Warning, TEXT("warrior=%s speed=%d"), *WarriorData.displayName, WarriorData.spd);
	GetMesh()->SetCollisionResponseToChannel(ECollisionChannel::ECC_Visibility, ECollisionResponse::ECR_Block);



}

// Called every frame
//void AWarriorBase::Tick(float DeltaTime)
//{
//	Super::Tick(DeltaTime);
//	if (CurrentTarget&&CurrentState==FCharacterState::StartAttack)
//	{
//		FRotator NewRotation = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), CurrentTarget->GetActorLocation());
//		if (NewRotation.Equals( GetActorRotation(),5)) 
//		{
//			CurrentState = FCharacterState::Attacking;
//			UE_LOG(LogTemp, Warning, TEXT("rotation ends, current phase is attacking"));
//		}
//		else
//		{
//			SetActorRotation(FMath::RInterpTo(GetActorRotation(), NewRotation, GetWorld()->DeltaTimeSeconds, RotationSpeed));
//		}
//		
//
//	}
//	
//
//}

// Called to bind functionality to input
void AWarriorBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

float AWarriorBase::GetBufModification(FStatusType StatusType)
{
	//UE_LOG(LogTemp, Warning, TEXT("getting state mod for state %s"), StatusType);
	float val = 1;
	for (FBuffData Buff : CurrentBuffs)
	{
		//UE_LOG(LogTemp, Warning, TEXT("current buff is %s"), Buff);
		if (Buff.BuffStatus == StatusType)
		{
			val *= Buff.StateModifier;
		}
	}
	return val;
}

void AWarriorBase::ActionFinished_Implementation(int32 Weight, bool bShouldResetCamera)
{
	if (!GameController)
	{
		return;
	}
	RemainingActions--;
	ActionWeight += Weight;
	CurrentTarget = nullptr;
	if (bShouldResetCamera)
		GameController->ResetCamera();
	if (RemainingActions <= 0)
	{
		
			GameController->ActivateNextTurn(false);
		
	}
	else if (bIsCPU)
	{
		RunAITurn();
		
	}
	else
	{
		GameController->ToggleBattleMenu(ESlateVisibility::Visible);
	}
}

void AWarriorBase::Move_Implementation(FVector Target)
{
}

void AWarriorBase::StartTurn_Implementation()
{
	TurnIndicator->SetVisibility(true);

	RemainingActions = MaxActions;
	ActionWeight = 0;
	PlayTurnStartIndicator();


}
void AWarriorBase::PlayTurnStartIndicator_Implementation()
{
}

void AWarriorBase::DoAction_Implementation()
{
	UE_LOG(LogTemp, Warning, TEXT("Action from %s"), *WarriorData.displayName);
	ActionWeight += FMath::RandRange(0, 15);
	RemainingActions--;
	GameController->ResetCamera();
	if (RemainingActions <= 0)
	{
		if (GameController)
		{

			GameController->ActivateNextTurn(false);
		}
	}
}

void AWarriorBase::IncrementActionMeter_Implementation(int32 Amount)
{
	ActionMeter += Amount + WarriorData.spd;
}

void AWarriorBase::RunAITurn_Implementation()
{
	UE_LOG(LogTemp, Warning, TEXT("Starting turn for  %s"), *WarriorData.displayName);
	AAIController* Bot = Cast<AAIController>(GetController());
	if (GameController)
	{
		UE_LOG(LogTemp, Warning, TEXT("Ending turn for  %s"), *WarriorData.displayName);
		GameController->ActivateNextTurn(false);

	}
}

void AWarriorBase::Attack_Implementation(AWarriorBase * Target)
{

	float AtkModifier = GetStateModifier(FStatusType::Attack);
	float DefenceModifier = Target->GetStateModifier(FStatusType::Defence);
	this->CurrentTarget = Target;
	LookAt(Target);
	CurrentState = FCharacterState::Attacking;

	UE_LOG(LogTemp, Warning, TEXT("atk=%d, atkMod=%f,Def=%d, defMod=%f"), WarriorData.atk, AtkModifier, Target->WarriorData.def, DefenceModifier);
	DamageAmount = (AtkModifier / 2) - (DefenceModifier / 4);
	DamageAmount = DamageAmount*(FMath::RandRange(0.8f, 1.2f));

	UE_LOG(LogTemp, Warning, TEXT("Dealing damage to  %s, amount=%f"), *Target->WarriorData.displayName, DamageAmount);

}

void AWarriorBase::ApplyDamage_Implementation(float DamageAmount,bool FinishAction)
{

	int32 EfectiveDamage = FMath::FloorToInt(DamageAmount);

	CurrentHP -= EfectiveDamage;
	DamageEvent.Broadcast(EfectiveDamage, GetActorLocation(), CurrentHP <= 0, FinishAction);
	//CurrentState = FCharacterState::Hit;
	if (CurrentHP <= 0)
	{
		CurrentState = FCharacterState::Dying;
		//GameController->WarriorDestroyed(this, bIsCPU);
	}
	
}

bool AWarriorBase::bIsTargetInRange_Implementation(AWarriorBase * Target)
{
	return false;
}



FTransform AWarriorBase::GetCameraTransform_Implementation()
{
	return FTransform();
}

void AWarriorBase::EndTurn()
{
	TurnIndicator->SetVisibility(false);
}

void AWarriorBase::AddBuff(const FBuffData Buff)
{

	if (CurrentBuffs.Contains(Buff))
	{
		UE_LOG(LogTemp, Warning, TEXT("Buff already exists for  %s, will skip adding"), *WarriorData.displayName);
		return;
	}
	CurrentBuffs.Add(Buff);

}

int32 AWarriorBase::GetStateModifier(FStatusType Status)
{
	int32 Value = 0;
	switch (Status)
	{
		case FStatusType::Attack:
			Value = (WarriorData.atk + Weapon.BasePower)*GetBufModification(Status);

			break;
		case FStatusType::Defence:
			Value = (WarriorData.def + Armor.BasePower)*GetBufModification(Status);
			break;
		case FStatusType::Wisdom:
			break;
		case FStatusType::Speed:
			break;

	}
	return Value;
}

bool AWarriorBase::IsCPU()
{
	return bIsCPU;
}

void AWarriorBase::LookAt(AWarriorBase * Target)
{
	if (Target == nullptr)
	{
		return;
	}
	FRotator NewRotation = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), Target->GetActorLocation());
	while (!NewRotation.Equals(GetActorRotation(), 5))
	{
		SetActorRotation(FMath::RInterpTo(GetActorRotation(), NewRotation, GetWorld()->DeltaTimeSeconds, RotationSpeed));
		NewRotation = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), Target->GetActorLocation());
	}

}



bool AWarriorBase::IsHPLower(float HPRange)
{
	return ((float)CurrentHP / (float)WarriorData.MaxHP) <= HPRange;
}

bool AWarriorBase::IsHPHigher(float HPRange)
{
	return ((float)CurrentHP / (float)WarriorData.MaxHP) >= HPRange;
}

bool AWarriorBase::HasBuffOfType(FStatusType BuffType)
{
	for (auto Buff : CurrentBuffs)
	{
		if (Buff.BuffStatus == BuffType)
		{
			return true;
		}
	}
	return false;
}
