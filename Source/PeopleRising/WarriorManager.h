// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "UObject/NoExportTypes.h"
#include "StructsManager.h"
#include "UObject/ConstructorHelpers.h"
#include "WarriorManager.generated.h"

/**
 * 
 */
UCLASS()
class PEOPLERISING_API UWarriorManager : public UObject
{
	GENERATED_BODY()
public:
	UWarriorManager();
	UFUNCTION()
		TArray<FWarriorData>loadWarriors(const TArray<FString>&warriorsIds);
	UFUNCTION()
		TMap<FString,FSkillData>loadSkills(const TArray<FString>&SkillIds);
	UFUNCTION()
		TArray<FWarriorData>loadInitialPlayerWarriors();
	UFUNCTION()
		FClassData loadClass(const FString ClassId);
	UFUNCTION()
		TMap<FString, FClassData> loadAvailableClasses(const FWarriorData& Warrior);
	UFUNCTION()
		FSkillData loadSkill(const FString SkillId);
	UFUNCTION()
		FBuffData loadBuff(const FString BuffId);
	UFUNCTION()
		FMeshData loadMesh(const FString MeshID);
	UFUNCTION()
		FWeaponData loadEquipment(const FString MeshID);
	UFUNCTION()
		FWarriorData ScaleWarriorStatus(const FClassData& Class, const FWarriorData& warrior);
	UFUNCTION()
		FWarriorData LevelupWarriorStatus(const FWarriorData& warrior);
	UFUNCTION()
		int32 CalculateEXP(const FWarriorData& warrior);
	UFUNCTION()
		int32 CalculateAP(const FWarriorData& warrior);
	UPROPERTY()
		UDataTable * WarriorsList;
	UPROPERTY()
		UDataTable * MeshLsit;
	UPROPERTY()
		UDataTable * EquipLsit;
	UPROPERTY()
		UDataTable * InitialWarriorsList;
	UPROPERTY()
		UDataTable * ClassesList;
	UPROPERTY()
		UDataTable * SkillsList;
	UPROPERTY()
		UDataTable * BuffList;

private:
	int32 AdjustIntStatus(int32 status, int32 upperBound, int32 lowerBound);
};
