// Fill out your copyright notice in the Description page of Project Settings.

#include "BattleController.h"
#include "Startegies/EffectApplyerObjectFactory.h"
#include "GameObjects/DamageDisplay.h"
#include "GameFramework/PlayerController.h"


#include "Elementalists.h"

ABattleController::ABattleController()
{
	Directions.Append(GetMovementDirections());

}
void ABattleController::SetUpMap_Implementation(int32 MapWidth, int32 MapHeight, const FTransform& StartingPlayerLocation,
	const TMap<FVector2D, FWarriorData>& EnemyWarriors, const TMap<FVector2D, FWarriorData>& PlayerWarriors)
{
	if (!WarriorClass || !GridClass)
	{
		return;
	}
	UtilityTimer Timer;
	Timer.tick();
	if (Directions.Num() < 1)
	{
		UE_LOG(LogTemp, Warning, TEXT("Constructor isn't called ********************************* "));
		Directions.Append(GetMovementDirections());
	}
	if (!EffectApplyerFactory)
	{
		EffectApplyerFactory = NewObject<UEffectApplyerObjectFactory>();
	}

	UWorld * World = GetWorld();
	if (World)
	{
		if (!Wmanager)
		{
			Wmanager = Cast<UBaseGameInstance>(World->GetGameInstance())->GetWarriorsManager();
		}
		if (!Skillmanager)
		{
			Skillmanager = Cast<UBaseGameInstance>(World->GetGameInstance())->GetSkillManager();
		}
		DrawGrid(World, StartingPlayerLocation, MapWidth, MapHeight, EnemyWarriors, PlayerWarriors);

		int32 durationMs = Timer.tock();
		UE_LOG(LogTemp, Warning, TEXT("time taken in spawn=  %d "), durationMs);

		AssetLoader.RequestAsyncLoad(ItemsToStream, FStreamableDelegate::CreateUObject(this, &ABattleController::FinishLoadingWarriors));


	}
}

AWarriorBase * ABattleController::GetCurrentWarrior_Implementation()
{
	return AllWarriors[0];
}

void ABattleController::SetupInputComponent()
{
	Super::SetupInputComponent();
	InputComponent->BindAction("ClickAction", IE_Pressed, this, &ABattleController::HandleClickAction);
	InputComponent->BindAction("Cancel", IE_Pressed, this, &ABattleController::HandleCancelAction);
	InputComponent->BindAction("Confirm", IE_Pressed, this, &ABattleController::HandleConfirmAction);
	InputComponent->BindAction("SpeedUp", IE_Pressed, this, &ABattleController::SpeedUp);
	InputComponent->BindAction("SpeedDown", IE_Pressed, this, &ABattleController::SpeedDown);
}

void ABattleController::BeginPlay()
{
	UtilityTimer Timer;
	Timer.tick();

	SetInputMode(FInputModeGameAndUI());
	bShowMouseCursor = true;
	InfoWidget = CreateWidget<UUserWidget>(GetWorld(), InfoWidgetClass);
	LoadingWidget = CreateWidget<UUserWidget>(GetWorld(), LoadingWidgetClass);

	if (InfoWidget != nullptr)
	{
		InfoWidget->AddToViewport();
		InfoWidget->SetVisibility(ESlateVisibility::Hidden);
	}
	BattleWidget = CreateWidget<UUserWidget>(GetWorld(), BattleWidgetClass);
	if (BattleWidget != nullptr)
	{
		BattleWidget->AddToViewport();
		BattleWidget->SetVisibility(ESlateVisibility::Hidden);
	}
	if (LoadingWidget != nullptr)
	{
		LoadingWidget->AddToViewport();
		LoadingWidget->SetVisibility(ESlateVisibility::Visible);
	}
	int32 durationMs = Timer.tock();
	UE_LOG(LogTemp, Warning, TEXT("time taken in begin play=  %d "), durationMs);

}

void ABattleController::UpdateOccupyingWarriorInGrid(FVector2D Coordinates, AWarriorBase * Target)
{
	if (GridMap.Contains(Coordinates))
	{
		AGridTile*TileToUpdate = GridMap[Coordinates];
		TileToUpdate->CurrentWarrior = Target;
	}
}



void ABattleController::ToggleBattleWidget()
{
}

void ABattleController::ShowDamage_Implementation(int32 DamageAmount, FVector DIsplayLocation,bool IsDead,bool ShouldNotifyController)
{
}

void ABattleController::ToggleBattleMenu(ESlateVisibility NewVisibility)
{
	OnVisibityChange.Broadcast();
	BattleWidget->SetVisibility(NewVisibility);
}

void ABattleController::ReorderWarriors()
{
	AllWarriors.Sort([](const AWarriorBase& A, const AWarriorBase& B)
	{
		return A.ActionMeter > B.ActionMeter;
	});

}

void ABattleController::HandleClickAction()
{
	AWarriorBase * CurrentWarrior = GetCurrentWarrior();
	if (!CurrentWarrior->bIsCPU)
	{


		FHitResult Result(ForceInit);
		bool bIsHit = this->GetHitResultUnderCursorByChannel(UEngineTypes::ConvertToTraceType(ECC_Visibility), false, Result);
		if (bIsHit)
		{
			AActor*Target = Result.GetActor();
			AGridTile * Grid = Cast<AGridTile>(Target);
			UE_LOG(LogTemp, Warning, TEXT("actor hit is  %s "), *Target->GetName());
			FVector Location = Result.Location;
			AWarriorBase*TargetWarrior = Cast<AWarriorBase>(Target);
			switch (CurrentPhase)
			{
				case EActionPhase::MOVE_TARGET_SELECT:
					if (Grid&&Grid->CanMoveTo(CurrentWarrior->CurrentLocation, Grid->Cordinates, CurrentWarrior->MovementRange) && !IsGridOccupied(Grid->Cordinates))
					{

						AGridTile* OriginalTile = GridMap[CurrentWarrior->CurrentLocation];
						OriginalTile->IsOccupied = false;
						OriginalTile->CurrentWarrior = nullptr;
						Grid->IsOccupied = true;
						CurrentWarrior->CurrentLocation = Grid->Cordinates;
						CurrentWarrior->Move(Grid->GetCenterLocation());
						Grid->CurrentWarrior = CurrentWarrior;
						CurrentPhase = EActionPhase::DEFAULT;
						for (auto Entry : GridMap)
						{
							Entry.Value->UnHighLightTile();
						}


					}

					break;
				case EActionPhase::ATTACK_TARGET_SELECT:

					if (TargetWarrior&&TargetWarrior != CurrentWarrior)
					{
						if (CurrentWarrior->bIsTargetInRange(TargetWarrior))
						{
							ToggleBattleMenu(ESlateVisibility::Hidden);
							CurrentWarrior->CurrentActionWeight = CurrentWarrior->AttackWeight;
							CurrentWarrior->Attack(TargetWarrior);
							CurrentPhase = EActionPhase::DEFAULT;

						}
					}
					break;
				case EActionPhase::SKILL_TARGET_SELECT:
					if (!CurrentSkill.isDummy && (Grid != nullptr || TargetWarrior != nullptr))
					{
						FVector2D CenterPoint;
						if (Grid)
						{
							CenterPoint = Grid->Cordinates;
						}
						else
						{
							CenterPoint = TargetWarrior->CurrentLocation;
						}

						TArray<AWarriorBase*>Targets = GetSkillTargets(CurrentSkill, CenterPoint, CurrentWarrior);
						bool shouldSkillMorph = Skillmanager->ShoulSkillMorph(CurrentSkill, CurrentWarrior, Targets);
						if (shouldSkillMorph)
						{
							CurrentSkill = Skillmanager->GetMorphSkill(CurrentSkill);

						}
						if (SkillExecutorClass != nullptr&&Targets.Num() > 0)
						{
							if (Executor == nullptr)
							{
								Executor = GetWorld()->SpawnActor<ASkillExecutor>(SkillExecutorClass, FVector::OneVector, FRotator::ZeroRotator);
							}

							if (Executor)
							{
								CurrentWarrior->CurrentActionWeight = CurrentSkill.ActionWeight;
								auto Effector = EffectApplyerFactory->GetEffectStartegyObject(CurrentSkill, GetWorld());
								OnActionStartEvent.Broadcast(FText::FromString(CurrentSkill.SkillName), GetNextCameraPoint(), shouldSkillMorph);
								Executor->ExecuteSkill(CurrentSkill, CurrentWarrior, Targets, Effector);
								CurrentPhase = EActionPhase::DEFAULT;

							}
						}
					}
					break;
				default:
					break;
			}
		}
	}
}
void ABattleController::HandleCancelAction()
{
	switch (CurrentPhase)
	{
		case EActionPhase::SELECT_ACTION:
			CurrentPhase = EActionPhase::VIEW_INFO;
			break;
		case EActionPhase::ATTACK_TARGET_SELECT:
			CurrentPhase = EActionPhase::SELECT_ACTION;
			break;
		case EActionPhase::MOVE_TARGET_SELECT:
			CurrentPhase = EActionPhase::SELECT_ACTION;
			break;
		case EActionPhase::SKILL_TARGET_SELECT:
			CurrentPhase = EActionPhase::SELECT_ACTION;
			break;

	}
}
void ABattleController::HandleConfirmAction()
{
}
void ABattleController::HandleNavigateAction()
{
}
void ABattleController::SpeedUp()
{
}
void ABattleController::SpeedDown()
{
	UGameplayStatics::SetGlobalTimeDilation(GetWorld(), 0.5);
}
void ABattleController::ResetCamera()
{
	ActionEnded.Broadcast();
}
FVector2D ABattleController::GetBestTargetPoint_Implementation(FVector2D Base, FVector2D Target, int32 MoveRange, int32 AttackRange)
{
	AGridTile* Grid = GridMap[Target];
	AGridTile* BaseGrid = GridMap[Base];

	if (Grid)
	{

		int32 shortestDistance = 10000;
		FVector2D NearestPoint;
		bool TargetPointFound = false;
		auto AvaialableGrids = Grid->GetAvailableMoves(Target, Directions, AttackRange);
		for (auto TargetLocation : AvaialableGrids)
		{
			int32 distance = (Base - TargetLocation).Size();
			if (distance < shortestDistance && !IsGridOccupied(TargetLocation))
			{
				TargetPointFound = true;
				shortestDistance = distance;
				NearestPoint = TargetLocation;


			}
		}

		shortestDistance = 10000;
		FVector2D NearestMovementPoint(-1, -1);
		if (TargetPointFound)
		{


			auto AvaialableMovementGrids = Grid->GetAvailableMoves(Base, Directions, MoveRange);
			for (auto TargetLocation : AvaialableMovementGrids)
			{
				int32 distance = (NearestPoint - TargetLocation).Size();
				if (distance < shortestDistance && !IsGridOccupied(TargetLocation))
				{
					shortestDistance = distance;
					NearestMovementPoint = TargetLocation;
				}
			}
		}
		AGridTile* TargetTile = GridMap[NearestMovementPoint];
		TargetTile->IsOccupied = true;
		BaseGrid->IsOccupied = false;
		return NearestMovementPoint;

	}

	return FVector2D();
}
FVector ABattleController::GetGridLocation_Implementation(FVector2D Base)
{
	if (GridMap.Contains(Base))
	{
		AGridTile* GridToMoveTo = GridMap[Base];

		return GridToMoveTo->GetCenterLocation();

	}
	return FVector();
}

void ABattleController::HieghlightValidMoves_Implementation(FVector2D CurrentLocation, int32 MoveRange)
{
	AGridTile* Grid = GridMap[CurrentLocation];
	if (Grid)
	{


		auto AvaialableGrids = Grid->GetAvailableMoves(CurrentLocation, Directions, MoveRange);
		for (auto TargetLocation : AvaialableGrids)
		{
			if (GridMap.Contains(TargetLocation))
			{
				AGridTile* TargetGrid = GridMap[TargetLocation];
				if (TargetGrid)
				{
					TargetGrid->HighLightTile();
				}
			}


		}
	}
}

AWarriorBase * ABattleController::GetBestTarget_Implementation(AWarriorBase * AIWarrior)
{
	return PlayerWarriorsArray[0];
}

void ABattleController::EventDone_Implementation()
{
}

void ABattleController::ActivateNextTurn_Implementation(bool bIsFirstTurn)
{

	AWarriorBase* CurretWarrior = AllWarriors[0];
	CurretWarrior->EndTurn();
	int32 Wieght = CurretWarrior->ActionWeight;
	if (!bIsFirstTurn)
	{
		//LogOrder();
		for (int i = 1; i < AllWarriors.Num(); i++)
		{
			auto * Warrior = AllWarriors[i];
			if (Warrior)
			{
				Warrior->IncrementActionMeter(Wieght);
			}

		}
		ReorderWarriors();
		//LogOrder();
	}
	CurretWarrior = AllWarriors[0];
	CurretWarrior->StartTurn();
	OnTurnStartEvent.Broadcast(GetNextCameraPoint());
	//SetViewTargetWithBlend(CurretWarrior, 1);




}
void ABattleController::WarriorTurnReady()
{
	AWarriorBase* CurretWarrior = AllWarriors[0];
	if (CurretWarrior->bIsCPU)
	{
		ToggleBattleMenu(ESlateVisibility::Hidden);
		UE_LOG(LogTemp, Warning, TEXT("inside turn ready for cpu"));
		CurretWarrior->RunAITurn();
	}
	else
	{
		ToggleBattleMenu(ESlateVisibility::Visible);
		UE_LOG(LogTemp, Warning, TEXT("inside turn ready for non cpu"));
	}
}

void ABattleController::FinishAction()
{
	GetCurrentWarrior()->ActionFinished(GetCurrentWarrior()->CurrentActionWeight);
}

void ABattleController::DoAttack_Implementation(AWarriorBase * Attacker, AWarriorBase * Target)
{
	if (Target)
	{

		Target->ApplyDamage(Attacker->DamageAmount);
		//Attacker->ActionFinished(Attacker->AttackWeight);
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("calling do attack too many times"));
	}
}






TArray<FWarriorData> ABattleController::LevelUpWarriors()
{
	TArray<FWarriorData>PlayerWarriorsData;
	PlayerWarriorsArray.Append(DefeatedPlayerWarriors);
	for (auto Warrior : PlayerWarriorsArray)
	{

		FWarriorData status = Warrior->WarriorData;
		status.AbiltiPoints += Results.AbilityPointsGained;
		int32 WarriorEXP = Results.ExperienceGained;
		int32 neededXpToLevelup = status.expToNextLevel - status.exp;
		while (neededXpToLevelup < WarriorEXP)
		{
			WarriorEXP -= neededXpToLevelup;
			status = Wmanager->LevelupWarriorStatus(status);
			status.exp += neededXpToLevelup;
			neededXpToLevelup = status.expToNextLevel - status.exp;
		}
		status.exp += WarriorEXP;
		PlayerWarriorsData.Add(status);
	}
	return PlayerWarriorsData;
}

TArray<FWarriorData> ABattleController::PlayerWarriors()
{
	TArray<FWarriorData> PlayerInitialWarriorsArray;
	for (AWarriorBase* WarriorObject : PlayerWarriorsArray)
	{
		FWarriorData WarriorData = WarriorObject->WarriorData;
		PlayerInitialWarriorsArray.Add(WarriorData);
	}
	return PlayerInitialWarriorsArray;
}

bool ABattleController::WarriorDestroyed(AWarriorBase * Warrior, bool bIsCPU)
{
	bool levelFinished=false;
	FVector2D WarriorCoordinates = Warrior->CurrentLocation;
	if (GridMap.Contains(WarriorCoordinates))
	{
		AGridTile* Tile = GridMap[WarriorCoordinates];
		Tile->IsOccupied = false;
	}
	if (bIsCPU)
	{
		CPUWarriros.Remove(Warrior);
		AllWarriors.Remove(Warrior);
		DefeatedCPUWarriors.Add(Warrior);
		Results.ExperienceGained += Wmanager->CalculateEXP(Warrior->WarriorData);
		Results.AbilityPointsGained += Wmanager->CalculateAP((Warrior->WarriorData));
		Results.MoneyGained = +Warrior->WarriorData.level * 5;

		Warrior->SetActorHiddenInGame(true);
		if (CPUWarriros.Num() <= 0)
		{
			levelFinished = true;
			
		}
	}
	else
	{
		PlayerWarriorsArray.Remove(Warrior);
		AllWarriors.Remove(Warrior);
		DefeatedPlayerWarriors.Add(Warrior);
		Warrior->SetActorHiddenInGame(true);
		if (PlayerWarriorsArray.Num() <= 0)
		{
			levelFinished = true;
		}
	}
	if (levelFinished)
	{
		LevelCompleted(bIsCPU);

	}
	return levelFinished;
}

void ABattleController::LevelCompleted(bool bIsWon)
{
	if (bIsWon)
	{
		UE_LOG(LogTemp, Warning, TEXT("event finished succesfully"));
		EventDone();
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("event lost"));
	}
}

FTransform ABattleController::adjustWarriorTransform(const FTransform & InitialTransform, int32 Index)
{
	FTransform NewTransform = InitialTransform;
	float NewY = NewTransform.GetLocation().Y + (Index* SpaceBetweenWarriors);
	NewTransform.SetLocation(FVector(NewTransform.GetLocation().X, NewY, NewTransform.GetLocation().Z));
	return NewTransform;
}
void ABattleController::AddWarriorProperties(AWarriorBase * WarriorToSpawn, FWarriorData warrior, bool IsCpu, FTransform WarriorTransform)
{
	WarriorToSpawn->GameController = this;
	FClassData ClassData = Wmanager->loadClass(warrior.Class);
	TArray<FSkillData>AssignedSkills;
	//TODO revert to assigned skills not all available skills
	Wmanager->loadSkills(warrior.AvailableSkills).GenerateValueArray(AssignedSkills);
	WarriorToSpawn->AssignedSkills = AssignedSkills;
	for (FSkillData Skill : AssignedSkills)
	{
		ItemsToStream.AddUnique(Skill.Animtation.ToSoftObjectPath());
		ItemsToStream.AddUnique(Skill.SFX.ToSoftObjectPath());
		ItemsToStream.AddUnique(Skill.VFX.ToSoftObjectPath());

		if (Skill.CanMorph)
		{
			FSkillData MorphSkill = Wmanager->loadSkill(Skill.MorphSkillPointer);
			if (!MorphSkill.isDummy)
			{

				ItemsToStream.AddUnique(MorphSkill.Animtation.ToSoftObjectPath());
				ItemsToStream.AddUnique(MorphSkill.SFX.ToSoftObjectPath());
				ItemsToStream.AddUnique(MorphSkill.VFX.ToSoftObjectPath());
			}
		}
	}
	FWeaponData Weapon = Wmanager->loadEquipment(warrior.EquippedWeapon);
	ItemsToStream.AddUnique(Weapon.Mesh.ToSoftObjectPath());
	WarriorToSpawn->Weapon = Weapon;
	FWeaponData Armor = Wmanager->loadEquipment(warrior.EquippedArmor);
	ItemsToStream.AddUnique(Armor.Mesh.ToSoftObjectPath());
	WarriorToSpawn->Armor = Armor;
	if (warrior.IsSpecialWarrior)
	{
		WarriorToSpawn->WarriorDefaultMesh = Wmanager->loadMesh(warrior.MeshPath).Mesh;
	}
	else
	{
		WarriorToSpawn->WarriorDefaultMesh = ClassData.Mesh;
	}
	WarriorToSpawn->WarriorData = warrior;
	WarriorToSpawn->ClassData = ClassData;
	WarriorToSpawn->AssetLoader = &AssetLoader;
	WarriorToSpawn->bIsCPU = IsCpu;
	AllWarriors.Add(WarriorToSpawn);
	if (IsCpu)
	{
		CPUWarriros.Add(WarriorToSpawn);
	}
	else
	{
		PlayerWarriorsArray.Add(WarriorToSpawn);
	}
	ItemsToStream.AddUnique(WarriorToSpawn->WarriorDefaultMesh.ToSoftObjectPath());
	UGameplayStatics::FinishSpawningActor(WarriorToSpawn, WarriorTransform);

	WarriorToSpawn->SpawnDefaultController();
	WarriorToSpawn->AutoPossessAI = EAutoPossessAI::PlacedInWorldOrSpawned;
}

TArray<AWarriorBase*> ABattleController::GetSkillTargets(FSkillData  Skill, FVector2D CenterPoint, AWarriorBase* Initiator)
{
	TArray<AWarriorBase*> Targets;
	FSkillRange SkillRange = Skill.RangeType;
	AGridTile* Grid = GridMap[CenterPoint];
	if (Grid != nullptr)
	{
		switch (SkillRange)
		{
			case FSkillRange::Cross:
			{


				auto AvaialableMovementGrids = Grid->GetAvailableMoves(CenterPoint, Directions, Skill.AttackRange);
				for (auto TargetLocation : AvaialableMovementGrids)
				{
					if (GridMap.Contains(TargetLocation))
					{
						AGridTile* NewGrid = GridMap[TargetLocation];
						AWarriorBase* Target = NewGrid->CurrentWarrior;
						if (Target)
							Targets.Add(Target);
					}

				}
				break;
			}
			default:

				AWarriorBase* Target = Grid->CurrentWarrior;
				if (Target)
					Targets.Add(Target);
				break;
		}
	}
	return Targets;
}

/*Drawing the battle grid*/
void ABattleController::DrawGrid(UWorld*World, const FTransform& InitialTransform, int32 width, int32 height, const TMap<FVector2D, FWarriorData>& EnemyWarriors, const TMap<FVector2D, FWarriorData>& PlayerWarriors)
{
	for (int32 i = 0; i < width; i++)
	{
		for (int32 j = 0; j < height; j++)
		{
			FVector2D Coordinates(i, j);
			FTransform NewTransform;

			NewTransform.SetLocation(FVector(InitialTransform.GetLocation().X + (i * 200), InitialTransform.GetLocation().Y + (j * 200), InitialTransform.GetLocation().Z));
			AGridTile*grid = World->SpawnActorDeferred<AGridTile>(GridClass, NewTransform);
			GridMap.Add(Coordinates, grid);
			grid->Cordinates = Coordinates;
			UGameplayStatics::FinishSpawningActor(grid, NewTransform);
			FWarriorData warrior;
			if (EnemyWarriors.Contains(Coordinates))
			{
				AWarriorBase* WarriorToSpawn = World->SpawnActorDeferred<AWarriorBase>(WarriorClass, NewTransform);
				NewTransform.SetLocation(FVector(grid->GetCenterLocation().X, grid->GetCenterLocation().Y, InitialTransform.GetLocation().Z));
				if (WarriorToSpawn)
				{
					warrior = EnemyWarriors[Coordinates];
					WarriorToSpawn->CurrentLocation = Coordinates;
					AddWarriorProperties(WarriorToSpawn, warrior, true, NewTransform);
					grid->CurrentWarrior = WarriorToSpawn;
					grid->IsOccupied = true;

				}
			}
			else if (PlayerWarriors.Contains(Coordinates))
			{

				NewTransform.SetLocation(FVector(grid->GetCenterLocation().X, grid->GetCenterLocation().Y, InitialTransform.GetLocation().Z));

				AWarriorBase* WarriorToSpawn = World->SpawnActorDeferred<AWarriorBase>(WarriorClass, NewTransform);

				if (WarriorToSpawn)
				{

					warrior = PlayerWarriors[Coordinates];
					WarriorToSpawn->CurrentLocation = Coordinates;
					AddWarriorProperties(WarriorToSpawn, warrior, false, NewTransform);
					grid->CurrentWarrior = WarriorToSpawn;
					grid->IsOccupied = true;

				}
			}

		}

	}
}
void ABattleController::LogOrder()
{
	for (int i = 0; i < AllWarriors.Num(); i++)
	{
		auto * Warrior = AllWarriors[i];

		if (Warrior)
		{
			UE_LOG(LogTemp, Warning, TEXT("action meter for  %s is %d"), *Warrior->WarriorData.displayName, Warrior->ActionMeter);
		}

	}
}

TArray<FVector2D> ABattleController::GetMovementDirections()
{
	TArray<FVector2D>MovementDirections;
	MovementDirections.Add(FVector2D::UnitVector);
	MovementDirections.Add(FVector2D(0, 1));
	MovementDirections.Add(FVector2D(1, 0));
	MovementDirections.Add(FVector2D(-1, 1));
	MovementDirections.Add(FVector2D(-1, 0));
	MovementDirections.Add(FVector2D(-1, -1));
	MovementDirections.Add(FVector2D(1, -1));
	MovementDirections.Add(FVector2D(0, -1));
	return MovementDirections;
}

void ABattleController::FinishLoadingWarriors()
{

	for (AWarriorBase* Warrior : AllWarriors)
	{
		Warrior->GetMesh()->SetSkeletalMesh(Warrior->WarriorDefaultMesh.Get());
		Warrior->WeaponMesh = Warrior->Weapon.Mesh.Get();
		Warrior->DamageEvent.AddDynamic(this, &ABattleController::ShowDamage);
		Warrior->OnTurnready.AddDynamic(this, &ABattleController::WarriorTurnReady);
	}

	if (LoadingWidget != nullptr)
	{
		LoadingWidget->SetVisibility(ESlateVisibility::Hidden);
		LoadingWidget->RemoveFromViewport();
	}
	ReorderWarriors();

	ActivateNextTurn(true);
}

FTransform ABattleController::GetNextCameraPoint()
{
	if (CameraPositions.Num() > 0)
	{
		int32 NearestDistance = 9999;
		FTransform NearestPoint;
		for (auto Entry : CameraPositions)
		{
			int32 VectorLength = FVector2D::Distance(Entry.Key, GetCurrentWarrior()->CurrentLocation);
			if (VectorLength < NearestDistance)
			{
				NearestPoint = Entry.Value;
			}
		}
		return NearestPoint;
	}
	return GetCurrentWarrior()->GetCameraTransform();
}

bool ABattleController::IsGridOccupied(FVector2D Target)
{
	if (GridMap.Contains(Target))
	{
		AGridTile* Tile = GridMap[Target];
		if (Tile && !Tile->IsOccupied)
		{
			return false;
		}
	}
	return true;
}
