// Fill out your copyright notice in the Description page of Project Settings.

#include "CameraPawn.h"
#include "Elementalists.h"


// Sets default values
ACameraPawn::ACameraPawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	 RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	 OurCameraSpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraSpringArm"));
	 OurCameraSpringArm->SetupAttachment(RootComponent);
	 OurCameraSpringArm->SetRelativeLocationAndRotation(FVector(0.0f, 0.0f, 50.0f), FRotator(-60.0f, 0.0f, 0.0f));
	 OurCameraSpringArm->TargetArmLength = 400.f;
	 OurCameraSpringArm->bEnableCameraLag = true;
	 OurCameraSpringArm->CameraLagSpeed = 3.0f;
	 OurCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("GameCamera"));
	 OurCamera->SetupAttachment(OurCameraSpringArm, USpringArmComponent::SocketName);
	 OurCamera->FieldOfView = 45;
	 //Take control of the default Player
	 AutoPossessPlayer = EAutoReceiveInput::Player0;

	  

}

// Called when the game starts or when spawned
void ACameraPawn::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ACameraPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	//Zoom in if ZoomIn button is down, zoom back out if it's not
	{
		if (bZoomingIn)
		{
			ZoomFactor += DeltaTime / 0.5f;         //Zoom in over half a second
		}
		else
		{
			ZoomFactor -= DeltaTime / 0.25f;        //Zoom out over a quarter of a second
		}
		ZoomFactor = FMath::Clamp<float>(ZoomFactor, 0.0f, 1.0f);
		//Blend our camera's FOV and our SpringArm's length based on ZoomFactor
		OurCamera->FieldOfView = FMath::Lerp<float>(90.0f, 60.0f, ZoomFactor);
		OurCameraSpringArm->TargetArmLength = FMath::Lerp<float>(400.0f, 300.0f, ZoomFactor);
	}
	//Rotate our actor's yaw, which will turn our camera because we're attached to it
	{
		FRotator NewRotation = GetActorRotation();
		NewRotation.Yaw += CameraInput.X;
		SetActorRotation(NewRotation);
	}

	//Rotate our camera's pitch, but limit it so we're always looking downward
	{
		FRotator NewRotation = OurCameraSpringArm->GetComponentRotation();
		NewRotation.Pitch = FMath::Clamp(NewRotation.Pitch + CameraInput.Y, -80.0f, -15.0f);
		OurCameraSpringArm->SetWorldRotation(NewRotation);
	}
	{
		if (!MovementInput.IsZero())
		{
			//Scale our movement input axis values by 100 units per second
			MovementInput = MovementInput.GetSafeNormal() * 500.0f;
			FVector NewLocation = GetActorLocation();
			NewLocation += GetActorForwardVector() * MovementInput.X * DeltaTime;
			NewLocation += GetActorRightVector() * MovementInput.Y * DeltaTime;
			NewLocation.Z += MovementInput.Z * DeltaTime;
			SetActorLocation(NewLocation);
		}
	}

}

// Called to bind functionality to input
void ACameraPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	InputComponent->BindAction("ZoomIn", IE_Pressed, this, &ACameraPawn::ZoomIn);
	InputComponent->BindAction("ZoomIn", IE_Released, this, &ACameraPawn::ZoomOut);

	//Hook up every-frame handling for our four axes
	InputComponent->BindAxis("MoveForward", this, &ACameraPawn::MoveForward);
	InputComponent->BindAxis("CameraUp", this, &ACameraPawn::MoveUp);
	InputComponent->BindAxis("MoveRight", this, &ACameraPawn::MoveRight);
	InputComponent->BindAxis("CameraPitch", this, &ACameraPawn::PitchCamera);
	InputComponent->BindAxis("CameraYaw", this, &ACameraPawn::YawCamera);
	// camera reset to default position
	InputComponent->BindAction("ResetCamera", IE_Pressed, this, &ACameraPawn::ResetCamera);
	ResetCamera();

}
//Input functions
void ACameraPawn::MoveForward(float AxisValue)
{
	MovementInput.X = FMath::Clamp<float>(AxisValue, -1.0f, 1.0f);
}

void ACameraPawn::MoveRight(float AxisValue)
{
	MovementInput.Y = FMath::Clamp<float>(AxisValue, -1.0f, 1.0f);
}

void ACameraPawn::MoveUp(float AxisValue)
{
	MovementInput.Z = FMath::Clamp<float>(AxisValue, -1.0f, 1.0f);
}

void ACameraPawn::PitchCamera(float AxisValue)
{
	CameraInput.Y = AxisValue;
}

void ACameraPawn::YawCamera(float AxisValue)
{
	CameraInput.X = AxisValue;
}

void ACameraPawn::ZoomIn()
{
	bZoomingIn = true;
}

void ACameraPawn::ZoomOut()
{
	bZoomingIn = false;
}
