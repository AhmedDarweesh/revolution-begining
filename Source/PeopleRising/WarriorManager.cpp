// Fill out your copyright notice in the Description page of Project Settings.

#include "WarriorManager.h"
#include "Elementalists.h"

	
UWarriorManager::UWarriorManager()
{
	static ConstructorHelpers::FObjectFinder<UDataTable>GameObjectLookupDataTable_BP(TEXT("DataTable'/Game/Data/WarriorData1.WarriorData1'"));

	WarriorsList = GameObjectLookupDataTable_BP.Object;
	static ConstructorHelpers::FObjectFinder<UDataTable>
		EquipListLookup(TEXT("DataTable'/Game/Data/Equips.Equips'"));
	EquipLsit = EquipListLookup.Object;
	static ConstructorHelpers::FObjectFinder<UDataTable>
		MeshListLookup(TEXT("DataTable'/Game/Data/MeshList.MeshList'"));

	MeshLsit = MeshListLookup.Object;
	static ConstructorHelpers::FObjectFinder<UDataTable>
		InitialWarriorsLookup(TEXT("DataTable'/Game/Data/InitialWarriorData.InitialWarriorData'"));

	InitialWarriorsList = InitialWarriorsLookup.Object;
	static ConstructorHelpers::FObjectFinder<UDataTable>
		ClassLookupDataTable_BP(TEXT("DataTable'/Game/Data/ClassData.ClassData'"));

	ClassesList = ClassLookupDataTable_BP.Object;


	static ConstructorHelpers::FObjectFinder<UDataTable>
		SkillLookupDataTable_BP(TEXT("DataTable'/Game/Data/SkillData.SkillData'"));

	SkillsList = SkillLookupDataTable_BP.Object;

	static ConstructorHelpers::FObjectFinder<UDataTable>
		BuffLookupDataTable_BP(TEXT("DataTable'/Game/Data/BuffData.BuffData'"));

	BuffList = BuffLookupDataTable_BP.Object;


}
static const FString ContextString(TEXT("GENERAL"));
TArray<FWarriorData> UWarriorManager::loadWarriors(const TArray<FString>& warriorsIds)
{
	TArray<FWarriorData> warriors;
	if (WarriorsList != nullptr)
	{
		for (auto&warriorId : warriorsIds)
		{
			FWarriorData* row = WarriorsList->FindRow<FWarriorData>(
				*warriorId,
				ContextString
				);
			if (row)
			{


				warriors.Add(*row);
			}
		}




	}

	return warriors;
}

TMap<FString, FSkillData> UWarriorManager::loadSkills(const TArray<FString>& SkillIds)
{
	TMap<FString, FSkillData> skills;
	if (SkillsList != nullptr)
	{
		for (auto&skillId : SkillIds)
		{
			FSkillData* row = SkillsList->FindRow<FSkillData>(*skillId, ContextString);
			if (row)
			{

				skills.Add(skillId, *row);
			}
		}

	}

	return skills;
}

TArray<FWarriorData> UWarriorManager::loadInitialPlayerWarriors()
{
	TArray<FWarriorData> warriors;
	for (auto RowName : InitialWarriorsList->GetRowNames())
	{
		FWarriorData* row = InitialWarriorsList->FindRow<FWarriorData>(RowName, ContextString);
		if (row)
		{
			auto PointerSize = sizeof(row);
			auto ObjectSize = sizeof(*row);
			UE_LOG(LogTemp, Warning, TEXT("pointer size=%d and object size=%d"), PointerSize, ObjectSize);

			warriors.Add(*row);
		}
	}


	return warriors;
}

FClassData UWarriorManager::loadClass(const FString ClassId)
{
	if (ClassesList != NULL)
	{

		FClassData* row = ClassesList->FindRow<FClassData>(*ClassId, ContextString);
		if (row)
		{

			return *row;
		}





	}
	return FClassData();
}

TMap<FString, FClassData> UWarriorManager::loadAvailableClasses(const FWarriorData& Warrior)
{
	TMap<FString, FClassData> availableClasses;
	auto AvailableSkills = Warrior.AvailableSkills;

	for (auto ClassId : ClassesList->GetRowNames())
	{
		FClassData*Row = ClassesList->FindRow<FClassData>(ClassId, ContextString);
		if (Row)
		{
			FClassData ClassData = *Row;
			if (Warrior.Class.Equals(ClassId.ToString(), ESearchCase::IgnoreCase))
			{
				continue;
			}
			switch (ClassData.RequirementType)
			{
				case FPreRequisiteForClass::Default:
					availableClasses.Add(ClassId.ToString(), ClassData);
					break;
				case FPreRequisiteForClass::Level:
					if (Warrior.level >= FCString::Atoi(*ClassData.RequirementValue))
					{
						availableClasses.Add(ClassId.ToString(), ClassData);
					}
					break;
				case FPreRequisiteForClass::Skill:
					if (AvailableSkills.Contains(ClassData.RequirementValue))
					{
						availableClasses.Add(ClassId.ToString(), ClassData);
					}
					break;
				default:
					break;
			}
		}
	}


	return availableClasses;
}

FSkillData UWarriorManager::loadSkill(const FString SkillId)
{
	if (SkillsList != nullptr)
	{

		FSkillData* row = SkillsList->FindRow<FSkillData>(*SkillId, ContextString);
		return *row;

	}


	return FSkillData(true);
}

FBuffData UWarriorManager::loadBuff(const FString BuffId)
{
	if (BuffList != nullptr)
	{
		FBuffData* row = BuffList->FindRow<FBuffData>(*BuffId, ContextString);
		if (row)
		{

			return *row;
		}
	}
	return FBuffData();
}

FMeshData UWarriorManager::loadMesh(const FString MeshID)
{
	if (MeshLsit != NULL)
	{

		FMeshData* row = MeshLsit->FindRow<FMeshData>(*MeshID, ContextString);
		if (row)
		{

			return *row;
		}





	}
	return FMeshData();
}

FWeaponData UWarriorManager::loadEquipment(const FString MeshID)
{
	if (EquipLsit != NULL)
	{

		FWeaponData* row = EquipLsit->FindRow<FWeaponData>(
			*MeshID,
			ContextString
			);
		if (row)
		{

			return *row;
		}





	}
	return FWeaponData();
}

FWarriorData UWarriorManager::ScaleWarriorStatus(const FClassData& Class, const FWarriorData& warrior)
{
	FWarriorData warriorAfterScale(warrior);
	if (warrior.overrideClassStatus)
	{
		int32 currentLevel = warrior.level;
		while (currentLevel > 1)
		{
			warriorAfterScale.atk = AdjustIntStatus(warrior.atk, Class.AtkGrowthRateMax, Class.AtkGrowthRateMin);
			warriorAfterScale.def = AdjustIntStatus(warrior.def, Class.DefGrowthRateMax, Class.DefGrowthRateMin);
			currentLevel--;
		}
	}
	return warriorAfterScale;

}

FWarriorData UWarriorManager::LevelupWarriorStatus(const FWarriorData & warrior)
{
	FWarriorData warriorAfterScale(warrior);
	FClassData WarriorClass = loadClass(warriorAfterScale.Class);
	warriorAfterScale.level++;
	warriorAfterScale.atk = AdjustIntStatus(warrior.atk, WarriorClass.AtkGrowthRateMax, WarriorClass.AtkGrowthRateMin);
	warriorAfterScale.def = AdjustIntStatus(warrior.def, WarriorClass.DefGrowthRateMax, WarriorClass.DefGrowthRateMin);
	warriorAfterScale.MaxHP = AdjustIntStatus(warrior.MaxHP, WarriorClass.HPGrowthRateMax, WarriorClass.HPGrowthRateMin);
	warriorAfterScale.MaxMP = AdjustIntStatus(warrior.MaxMP, WarriorClass.MPGrowthRateMax, WarriorClass.MPGrowthRateMin);
	warriorAfterScale.spd = AdjustIntStatus(warrior.spd, WarriorClass.SpdGrowthRateMax, WarriorClass.SpdGrowthRateMin);
	warriorAfterScale.wis = AdjustIntStatus(warrior.wis, WarriorClass.WisGrowthRateMax, WarriorClass.WisGrowthRateMin);
	warriorAfterScale.expToNextLevel = FMath::FloorToInt((warriorAfterScale.level * 100) + FMath::Loge(warriorAfterScale.level));
	return warriorAfterScale;
}

int32 UWarriorManager::CalculateEXP(const FWarriorData & warrior)
{
	int XP = FMath::Pow(warrior.level, 2) * 20;
	return XP;
}

int32 UWarriorManager::CalculateAP(const FWarriorData & warrior)
{
	return FMath::RandRange(warrior.level, warrior.level * 2) * 5;
}

int32 UWarriorManager::AdjustIntStatus(int32 status, int32 upperBound, int32 lowerBound)
{

	return status + (FMath::RandRange(lowerBound, upperBound));
}

