// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/PlayerController.h"
#include "Blueprint/UserWidget.h"
#include "WarriorBase.h"
#include "Startegies/EffectApplyerObjectFactory.h"
#include "GameObjects/GridTile.h"
#include "GameObjects/SkillExecutor.h"
#include "engine/StaticMesh.h"
#include "StructsManager.h"
#include "GameFrameworkObjects/BaseGameInstance.h"
#include "BattleController.generated.h"

/**
 * 
 */
UENUM(BlueprintType)		//"BlueprintType" is essential to include
enum class EActionPhase : uint8
{
	MOVE_TARGET_SELECT 	UMETA(DisplayName = "Move To"),
	ATTACK_TARGET_SELECT 	UMETA(DisplayName = "Select Attack Target"),
	SKILL_TARGET_SELECT 	UMETA(DisplayName = "Select Skill Target"),
	SELECT_ACTION 	UMETA(DisplayName = "Select Action"),
	VIEW_INFO 	UMETA(DisplayName = "View Info"),
	DEFAULT	UMETA(DisplayName = "Default")
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnTurnStart, FTransform, NewTransform);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnTurnEnd);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnActionExecutionStart, FText, CurrentActionText,FTransform,CameraNewTransform,bool,IsMorphedSkill);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnWidgetVisibiltyChange);
UCLASS()
class PEOPLERISING_API ABattleController : public APlayerController
{
	GENERATED_BODY()
	
public:
	ABattleController();

	//Properties//
	//UMG classes
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "UMG")
		TSubclassOf<UUserWidget> BattleWidgetClass;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "UMG")
		TSubclassOf<UUserWidget> InfoWidgetClass;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "UMG")
		TSubclassOf<UUserWidget> LoadingWidgetClass;
	//UMG windows
	UPROPERTY()
		UUserWidget* InfoWidget;
	UPROPERTY()
		UUserWidget* BattleWidget;
	UPROPERTY()
		UUserWidget* LoadingWidget;

	//Level actors classes definition
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Level Setup")
		TSubclassOf<AWarriorBase> WarriorClass;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Level Setup")
		TSubclassOf<AGridTile> GridClass;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "GamePlay Propertires")
		TSubclassOf<ASkillExecutor> SkillExecutorClass;

	//Level initial properties
	UPROPERTY( BlueprintReadWrite, Category = "Level Setup")
		TMap<FVector2D, FTransform> CameraPositions;
	
	FStreamableManager AssetLoader;
	//battle properties
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Battle Actions")
		EActionPhase CurrentPhase;
	UPROPERTY()
		UEffectApplyerObjectFactory* EffectApplyerFactory;
	UPROPERTY(BlueprintReadWrite, Category = "Battle Actions")
		FSkillData CurrentSkill;

	// events
	UPROPERTY(BlueprintAssignable)
		FOnTurnStart OnTurnStartEvent;
	UPROPERTY(BlueprintAssignable)
		FOnActionExecutionStart OnActionStartEvent;
	UPROPERTY(BlueprintAssignable)
		FOnWidgetVisibiltyChange OnVisibityChange;
	UPROPERTY(BlueprintAssignable)
		FOnTurnEnd ActionEnded;

	void ToggleBattleWidget();
	


	//functions//
	//Setup
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Level Setup")
		void SetUpMap(int32 MapWidth,int32 MapHeight,const FTransform& StartingPlayerLocation, const TMap<FVector2D,FWarriorData>& EnemyWarriors, const TMap<FVector2D, FWarriorData>& PlayerWarriors);
	//overrides
	virtual void BeginPlay();
		virtual void SetupInputComponent() override;
	//BattleEvents
		UFUNCTION(BlueprintCallable, Category = "Battle Actions")
			void UpdateOccupyingWarriorInGrid(FVector2D Coordinates, AWarriorBase* Target);
		UFUNCTION(BlueprintCallable, Category = "Battle Actions")
			void FinishAction();
	UFUNCTION(BlueprintCallable, Category = "Battle Actions")
		void WarriorTurnReady();
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Batlle Actions")
		void ActivateNextTurn(bool bIsFirstTurn);

	UFUNCTION(BlueprintCallable, Category = "Level Finished")
		bool WarriorDestroyed(AWarriorBase*Warrior, bool bIsCPU);
	UFUNCTION(BlueprintCallable, Category = "Level Finished")
		void LevelCompleted(bool bIsWon);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Level Finished")
		void EventDone();
	//BattleData
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Battle Actions")
		AWarriorBase * GetCurrentWarrior();
	UFUNCTION()
		void ReorderWarriors();
	UFUNCTION(BlueprintCallable, Category = "Player Data")
		TArray<FWarriorData> PlayerWarriors();
	// BattleActions	
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Battle Actions")
		void ShowDamage(int32 DamageAmount,FVector DIsplayLocation,bool IsDead, bool ShouldNotifyController);
	UFUNCTION( BlueprintCallable, Category = "Battle Actions")
		void ToggleBattleMenu(ESlateVisibility NewVisibility);
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Batlle Actions")
		void DoAttack(AWarriorBase* Attacker, AWarriorBase* Target);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Battle Actions")
		void HieghlightValidMoves(FVector2D CurrentLocation, int32 MoveRange);

	UFUNCTION(BlueprintCallable, Category = "Level Finished")
		TArray<FWarriorData> LevelUpWarriors();

	//InputHandle
	UFUNCTION()
		void HandleClickAction();
	UFUNCTION()
		void HandleCancelAction();
	UFUNCTION()
		void HandleConfirmAction();
	UFUNCTION()
		void HandleNavigateAction();
	UFUNCTION()
		void SpeedUp();
	UFUNCTION()
		void SpeedDown();
	UFUNCTION(BlueprintCallable, Category = "Batlle Actions")
		void ResetCamera();
	
	
	

	//AI
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Batlle Actions")
		AWarriorBase* GetBestTarget(AWarriorBase* AIWarrior);
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Batlle Actions")
		FVector2D GetBestTargetPoint(FVector2D Base,FVector2D Target, int32 MoveRange, int32 AttackRange);
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Batlle Actions")
		FVector GetGridLocation(FVector2D Base);
	

	
	

	

	
	
private:
	FTransform adjustWarriorTransform(const FTransform& InitialTransform, int32 Index);
	UFUNCTION()
		void AddWarriorProperties(AWarriorBase* Warrior, FWarriorData warriordata, bool IsCpu,FTransform WarriorTransform);
	UFUNCTION()
		TArray<AWarriorBase *>GetSkillTargets(FSkillData  Skill, FVector2D CenterPoint,  AWarriorBase* Initiator);
	UFUNCTION()
		void DrawGrid(UWorld*World, const FTransform& InitialTransform, int32 width, int32 height,  const TMap<FVector2D, FWarriorData>& EnemyWarriors, const TMap<FVector2D, FWarriorData>& PlayerWarriors);
	void LogOrder();
	TArray<FVector2D>GetMovementDirections();
	UFUNCTION()
		void FinishLoadingWarriors();
	UFUNCTION()
		FTransform GetNextCameraPoint();
	int32 SpaceBetweenWarriors = 250;
	UPROPERTY()
	TArray<AWarriorBase *>AllWarriors;
	UPROPERTY()
	ASkillExecutor* Executor;
	UPROPERTY()
	TArray<AWarriorBase *>DefeatedCPUWarriors;
	UPROPERTY()
	TArray<AWarriorBase *>DefeatedPlayerWarriors;
	UPROPERTY()
	TArray<AWarriorBase *>CPUWarriros;
	UPROPERTY()
	TArray<AWarriorBase *>PlayerWarriorsArray;	
	int32 MaxActions = 15;
	UPROPERTY()
	int32 CurrentRequiredAnimationMesages = 1;
	UPROPERTY()
		FBattleSpoils Results;
	UPROPERTY()
		UWarriorManager*Wmanager;
	UPROPERTY()
		USkillManager*Skillmanager;
	UPROPERTY()
		TArray<FSoftObjectPath> ItemsToStream;
	UPROPERTY()
		TMap<FVector2D, AGridTile *>GridMap;
	FCriticalSection m_mutex;
	TArray<FVector2D>Directions;
	bool IsGridOccupied(FVector2D Target);
};

class UtilityTimer
{
	int64 TickTime = 0;
	int64 TockTime = 0;
public:
	int64 unixTimeNow()
	{
		FDateTime timeUtc = FDateTime::UtcNow();
		return timeUtc.ToUnixTimestamp() * 1000 + timeUtc.GetMillisecond();
	}

	void tick()
	{
		TickTime = unixTimeNow();
	}

	int32 tock()
	{
		TockTime = unixTimeNow();
		return TockTime - TickTime;
	}
};