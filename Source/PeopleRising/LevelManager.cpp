// Fill out your copyright notice in the Description page of Project Settings.

#include "LevelManager.h"
#include "Elementalists.h"


ULevelManager::ULevelManager() {
	//Wmanager= ConstructObject<UWarriorManager>(UWarriorManager::StaticClass());
	Wmanager = NewObject<UWarriorManager>();
	
}


TArray<FWarriorData> ULevelManager::loadWarriors_Implementation(const TArray<FString>&  WarriorsNames)
{
	TArray<FWarriorData> warriors;
	if (!Wmanager) {
		Wmanager = NewObject<UWarriorManager>();
	}
	if (Wmanager) {

		warriors= Wmanager->loadWarriors(WarriorsNames);
	}
	
		return warriors;
	
}
TArray<FWarriorData> ULevelManager::loadDefaultWarriors_Implementation()
{
	TArray<FWarriorData> warriors;
	if (!Wmanager) {
		Wmanager = NewObject<UWarriorManager>();
	}
	if (Wmanager) {

		warriors = Wmanager->loadInitialPlayerWarriors();
	}

	return warriors;

}

FLevelData ULevelManager::loadLevelData_Implementation(const FString& levelName)
{
	FLevelData* row = LevelList->FindRow<FLevelData>(
		*levelName,
		TEXT("GENERAL")
		);
	if (row) {
		return *row;
		/*TArray<FString>events = row->Events;
		for (auto &eventName:events)
		{
			FEventData eventData = GetFirstAvailableEvent_Implementation(eventName);
			if (eventData.HasCutScene &&eventData.CutScene.IsValid()) {
				ULevelSequence* sequence= eventData.CutScene.Get();
				ULevelSequencePlayer* player = ULevelSequencePlayer::CreateLevelSequencePlayer(GEngine->GetWorld(), sequence, settings);
				player->Play();
				
			}

		}*/
		
	}
	return FLevelData();

}
FEnemiesFormation ULevelManager::loadLevelEnemies_Implementation(const FLevelData& level)
{
	TArray<int32>odds;

	for (int i = 0; i < level.EnemiesForamtions.Num(); i++) {
		for (int j = 0; j < level.EnemyOdds[i]; j++) {
			odds.Add(i);
		}
}
	FString formation =level.EnemiesForamtions[odds[FMath::RandRange(0, odds.Num())]];
	FEnemiesFormation* row = EnemiesFormations->FindRow<FEnemiesFormation>(
		*formation,
		TEXT("GENERAL")
		);
	if (row) {
				
		return *row;
		

	}
	return FEnemiesFormation();

}
FEventData ULevelManager::GetFirstAvailableEvent_Implementation(const FLevelData & row, const TArray<FString>&  FinishedEvenets)
{
	
	
	for (auto& currentevent : row.Events) {
		FEventData* eventName = EventsList->FindRow<FEventData>(*currentevent,TEXT("GENERAL"));
		if (eventName&&!FinishedEvenets.Contains(eventName->EventId)) {
			if (HasAllRequiredEventsDone(FinishedEvenets,eventName->RequiredEvents)) {
				return *eventName;
			}
			
		}
		}
		
	
	
	return  FEventData(true);
}
FClassData ULevelManager::loadClassData_Implementation(const FString& ClassId)
{
	if (!Wmanager) {
		Wmanager = NewObject<UWarriorManager>();
	}
	if (!Wmanager) {
		return FClassData();
	}
	return Wmanager->loadClass(ClassId);
}
FWarriorData ULevelManager::ScaleWarriorStatus_Implementation(const FClassData& Class, const FWarriorData& warrior)
{
	if (!Wmanager) {
		Wmanager = NewObject<UWarriorManager>();
	}
	if (Wmanager) {
		return Wmanager->ScaleWarriorStatus(Class, warrior);
	}
	return FWarriorData();
}
TMap<FString, FClassData> ULevelManager::loadAvailableClasses(const FWarriorData Warrior)
{
	if (!Wmanager) {
		Wmanager = NewObject<UWarriorManager>();
	}
	return Wmanager->loadAvailableClasses(Warrior);
}
TMap<FString, FSkillData> ULevelManager::loadClassSkills(const TArray<FString> SkillIds)
{
	if (!Wmanager) {
		Wmanager = NewObject<UWarriorManager>();
	}
	return Wmanager->loadSkills(SkillIds);
}

FWarriorData ULevelManager::LearnSkill(const FWarriorData Input, const FString NewSkill)
{
	FWarriorData NewWarrior (Input);
	TArray<FString>LearnedSkills = NewWarrior.AvailableSkills;
	auto Skill = Wmanager->loadSkill(NewSkill);
	NewWarrior.AbiltiPoints = NewWarrior.AbiltiPoints - Skill.SPCost;
	LearnedSkills.Add(NewSkill);
	NewWarrior.AvailableSkills = LearnedSkills;
	return NewWarrior;
}

bool ULevelManager::HasAllRequiredEventsDone(const TArray<FString>&  FinishedEvenets, const TArray<FString>&  RequiredEvents) {
	
	if (RequiredEvents.Num()>0) {
		
	
	for (auto& eventId:RequiredEvents)
	{
		if (!FinishedEvenets.Contains(eventId)) {
			return false;
		}
	}
	}
	return true;
}


