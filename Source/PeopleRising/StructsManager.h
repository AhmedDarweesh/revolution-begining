// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "UObject/NoExportTypes.h"
#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Runtime/LevelSequence/Public/LevelSequence.h" 
#include "Engine/DataTable.h"
#include "Sound/SoundCue.h"
#include "StructsManager.generated.h"


/**
 * 
 */
UCLASS()
class PEOPLERISING_API UStructsManager : public UObject
{
	GENERATED_BODY()
	
	
	
	
};
UENUM(BlueprintType)
enum class FPreRequisiteForClass : uint8 {
	Skill, Event, Level,Default
};
UENUM(BlueprintType)
enum class FEquipType : uint8 {
	Weapon, Armor, Accessory, Shield
};
UENUM(BlueprintType)
enum class FWeaponType : uint8 {
	Sword, Spear, Axe, Claw,Dagger,Clup,Bow
};
UENUM(BlueprintType)
enum class FSkillType : uint8 {
	Damage, Buff, Debuff, StatusInflict,StatusHeal, Heal,Passive
};
UENUM(BlueprintType)
enum class FSkillTarget : uint8 {
	Enemy, Ally, Self
};
UENUM(BlueprintType)
enum class FSkillRange : uint8 {
	Single,All,Radius,Cross,Line
};
UENUM(BlueprintType)
enum class FDamageType : uint8 {
	Physical, Magical,Fixed,Hybrid
};
UENUM(BlueprintType)
enum class FSpawnLocationType : uint8 {
	Caster, Target, All
};
UENUM(BlueprintType)
enum class FCharacterState : uint8 {
	Idle,Attacking, Moving, Hit, Dying,StartAttack
};
UENUM(BlueprintType)
enum class FElement : uint8 {
	Fire,Water,Wind,Earth,Light,Dark, NonElemental
};
UENUM(BlueprintType)
enum class FStatusType : uint8 {
	Attack, Defence, Wisdom, Speed, All
};

UENUM(BlueprintType)
enum class FSkillMorphType : uint8 {
	HPLower, HPHigher, AlliesNumber, ElementPercentage, NumberOfUses,HasBuff,HasDebuff,HasAilment,TargetHasAilment,TargetHasBuff,TargetHasDebuff,TileElement
};

//Structs
USTRUCT(BlueprintType)
struct FEnemiesFormation : public FTableRowBase
{

	GENERATED_USTRUCT_BODY()
public:
	
	UPROPERTY(EditAnywhere, Category = Base)
		TArray<FString> Enemies;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Base)
		TArray<int32> InitailLocations;
	

};
USTRUCT(BlueprintType)
struct FLevelData : public FTableRowBase
{

	GENERATED_USTRUCT_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Base)
		bool IsTown=true;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Base)
		bool ShouldScaleEnemeyLevels=true;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Base)
		TArray<int32> Setup;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Base)
		TArray<int32> Dimensions;
	UPROPERTY(EditAnywhere,  Category = Base)
		TArray<FString> EnemiesForamtions;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Base)
		TArray<FString> Events;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Base)
		TArray<int32> EnemyOdds;

};
USTRUCT(BlueprintType)
struct FEventData : public FTableRowBase
{
	FEventData() {
		isDummy = false;
	}
	FEventData(bool BIsDummy) {
		isDummy = BIsDummy;
	}
	GENERATED_USTRUCT_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Loading)
		bool isDummy;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Loading)
		FString EventId;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Loading)
		bool ShouldScaleEnemeyLevels;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Loading)
		bool IsBattleEvent;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Loading)
		bool HasCutScene;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Loading)
		TAssetPtr<ULevelSequence> CutScene;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
		TArray<FString> Enemies;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Loading)
		TArray<FString> RequiredEvents;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
		TArray<int32> InitailLocations;

};
USTRUCT(BlueprintType)
struct FClassData : public FTableRowBase
{

	GENERATED_USTRUCT_BODY()
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Base)
		TAssetPtr<USkeletalMesh> Mesh;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Appearance)
		FString MeshPath;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Status)
		int32 MovementRange=3;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Base)
		int32 BaseHP=30;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Base)
		int32 BaseMP=10;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Base)
		TArray<FString> AllSkills;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Base)
		int32 BaseAtk;
	UPROPERTY(EditAnywhere, Category = Base)
		int32 BaseDef;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Base)
		int32 BaseWis;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Base)
		int32 BaseSpd;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Base)
		int32 HPGrowthRateMax;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Base)
		int32 HPGrowthRateMin;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Base)
		int32 MPGrowthRateMax;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Base)
		int32 MPGrowthRateMin;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Base)
		int32 AtkGrowthRateMax;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Base)
		int32 AtkGrowthRateMin;
	UPROPERTY(EditAnywhere, Category = Base)
		int32 DefGrowthRateMax;
	UPROPERTY(EditAnywhere, Category = Base)
		int32 DefGrowthRateMin;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Base)
		int32 WisGrowthRateMax;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Base)
		int32 WisGrowthRateMin;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Base)
		int32 SpdGrowthRateMax;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Base)
		int32 SpdGrowthRateMin;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Base)
		FPreRequisiteForClass RequirementType;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Base)
		FString RequirementValue;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Base)
		FString displayName;

};
USTRUCT(BlueprintType)
struct FWeaponData : public FTableRowBase
{

	GENERATED_USTRUCT_BODY()
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Base)
		TAssetPtr<UStaticMesh> Mesh;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Base)
		TAssetPtr<USkeletalMesh> AnimatedMesh;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Base)
		int32 BasePower;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Base)
		int32 AttackRange;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Base)
		FName SlotName;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Base)
	FEquipType Type;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Base)
		FWeaponType WeaponType;



};


USTRUCT(BlueprintType)
struct FSkillMorphInfo : public FTableRowBase
{

	GENERATED_USTRUCT_BODY()
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Base)
		FSkillMorphType MorphType;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Base)
		float HPRange;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Base)
		FStatusType BuffStatus;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Base)
		int32 AlliesCount;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Base)
		int32 UsageCount;
	



};

USTRUCT(BlueprintType)
struct FBuffData : public FTableRowBase
{

	GENERATED_USTRUCT_BODY()
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Base)
		FStatusType BuffStatus;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Base)
		float StateModifier;	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Base)
		int32 NumberOfTurns;
public:
	inline bool operator==(const FBuffData& arg) const
	{
		return BuffStatus == arg.BuffStatus;
	}
	/*bool operator==(const FIntMargin& Other) const
	{
		return (Left == Other.Left) && (Right == Other.Right) && (Top == Other.Top) && (Bottom == Other.Bottom);
	}*/




};
USTRUCT(BlueprintType)
struct FSkillData : public FTableRowBase
{
	FSkillData() {
		isDummy = true;
	}
	FSkillData(bool BIsDummy) {
		isDummy = BIsDummy;
	}
	GENERATED_USTRUCT_BODY()
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Identifier)
		FString SkillName;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Identifier)
		bool isDummy;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Identifier)
		FText SkillDescription;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Identifier)
		FString SkillClass;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Identifier)
		FElement SkillElement;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Identifier)
		FDamageType SkillDamageType;
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Effects)
		TAssetPtr<UAnimMontage> Animtation;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Effects)
		TAssetPtr<UParticleSystem> VFX;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Effects)
		TAssetPtr<USoundCue>SFX;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = GameProperties)
		float BasePower;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = GameProperties)
		FString BuffPointer;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = GameProperties)
		int32 MPCost;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = GameProperties)
		int32 SPCost;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = GameProperties)
		int32 ActionWeight;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = GameProperties)
		int32 AttackRange;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = GameProperties)
		FSkillRange RangeType;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = MorphProperties)
		FSkillMorphInfo MorphData;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = MorphProperties)
		bool CanMorph;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = MorphProperties)
		FString MorphSkillPointer;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = SpawnProperties)
		FSkillTarget Target;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = GameProperties)
		FSkillType SkillType;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = GameProperties)
		bool bIsUnique;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = GameProperties)
		bool bIsProjectile;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = SpawnProperties)
		FSpawnLocationType LocationType;

	




};
USTRUCT(BlueprintType)
struct FItemData : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Identifier)
		FString ItemName;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Identifier)
		FText ItemDescription;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Identifier)
		FString LinkedSkill;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Effects)
		TAssetPtr<UAnimMontage> Animtation;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Effects)
		TAssetPtr<UParticleSystem> VFX;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Effects)
		TAssetPtr<USoundCue>SFX;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = GameProperties)
		int32 BuyPrice;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = GameProperties)
		int32 SellPrice;
	





};
USTRUCT(BlueprintType)
struct FMeshData : public FTableRowBase
{

	GENERATED_USTRUCT_BODY()
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Base)
		TAssetPtr<USkeletalMesh> Mesh;

	


};
USTRUCT(BlueprintType)
struct FIconData : public FTableRowBase
{

	GENERATED_USTRUCT_BODY()
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Base)
		TAssetPtr<UTexture2D> Icon;




};
USTRUCT(BlueprintType)
struct FWarriorData : public FTableRowBase
{

	GENERATED_USTRUCT_BODY()
	UPROPERTY( EditAnywhere, BlueprintReadWrite, Category = Appearance)
		FString MeshPath;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Appearance)
		FString IconPath;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Appearance)
		bool IsSpecialWarrior;
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Status)
		int32 MaxHP;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Status)
		int32 MaxMP;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Skills)
		TArray<int32> LearnedSkills;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Status)
		int32 atk;
	UPROPERTY(EditAnywhere, Category = Status)
		int32 def;
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Status)
		int32 wis;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Status)
		int32 spd;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Status)
		int32 MaxActions;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Status)
		int32 RemainingActions;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Class)
	FString Class;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Class)
		bool overrideClassStatus;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Status)
		int32 level;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Status)
		int32 exp;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Status)
		int32 expToNextLevel;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Status)
		int32 AbiltiPoints;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Status)
		int32 MovementRange;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Appearance)
		FString displayName;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Status)
		FElement Element;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Equipment)
		FString EquippedWeapon;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Equipment)
		FString EquippedArmor;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Equipment)
		FString EquippedAccessory;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Equipment)
		FString EquippedShield;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Skills)
		TArray<FString> AvailableSkills;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Skills)
		TArray<FString> AssignedSkills;

};
USTRUCT(BlueprintType)
struct FBattleSpoils
{
	GENERATED_BODY();
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<FString>Items;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 ExperienceGained;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 AbilityPointsGained;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 MoneyGained;
};
