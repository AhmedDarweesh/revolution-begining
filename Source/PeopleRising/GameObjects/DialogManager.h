// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "DialogManager.generated.h"

UCLASS()
class PEOPLERISING_API ADialogManager : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ADialogManager();
	void SetDialogLines(TArray<FText>DialogLines);
	void DisplayCurrentLine();
	void NextLine();
	void PreviousLine();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
private:
	TArray<FText>DialogLines;
	int32 DialogIndex();
	
	
};
