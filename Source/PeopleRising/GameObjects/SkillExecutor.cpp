// Fill out your copyright notice in the Description page of Project Settings.

#include "SkillExecutor.h"


// Sets default values
ASkillExecutor::ASkillExecutor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

void ASkillExecutor::ExecuteSkill_Implementation(FSkillData Skill, AWarriorBase * InitiatorWarrior,const TArray<AWarriorBase *>& Targets, UObject* Effector)
{
	if (!InitiatorWarrior || Targets.Num()<1)
	{
		return;
	}
	SkillTargets = Targets;
	CurrentSkill = Skill;
	Initiator = InitiatorWarrior;
	if(Effector)
	EffectApplyer = Effector;
	InitiatorWarrior->LookAt(Targets[0]);
	UE_LOG(LogTemp, Warning, TEXT("getting montage"));
	auto* Montage = Skill.Animtation.Get();
	if (Montage == nullptr)
	{
		UE_LOG(LogTemp, Warning, TEXT("Montage isn't created"));
		ApplyEffect();
	}
	UE_LOG(LogTemp, Warning, TEXT("Montage is created"));
	InitiatorWarrior->PlayAnimMontage(Montage);
	InitiatorWarrior->EffectStart.RemoveAll(this);
	InitiatorWarrior->EffectStart.AddDynamic(this, &ASkillExecutor::ShowVFX);

}
void ASkillExecutor::ShowVFX_Implementation()
{
}
void ASkillExecutor::ApplyEffect_Implementation()
{
	//UE_LOG(LogTemp, Warning, TEXT("action meter for  %s is %d"), *Warrior->WarriorData.displayName, Warrior->ActionMeter);
	
	if (EffectApplyer!=nullptr)
	{
		UE_LOG(LogTemp, Warning, TEXT("Start applying effect"));
		IEffectApplyerStartegy* IEffector = Cast<IEffectApplyerStartegy>(EffectApplyer);
		bool FinishAction = true;
		for (AWarriorBase* SkillTarget : SkillTargets)
		{
		if (IEffector&&Initiator&&SkillTarget)
		{
			IEffector->Execute_ApplyEffect(EffectApplyer, Initiator, SkillTarget, CurrentSkill,FinishAction);
			FinishAction = false;
		}

		}
	}
	else 
	{
		UE_LOG(LogTemp, Warning, TEXT("Object is null"));
	}
	
	
}

// Called when the game starts or when spawned
void ASkillExecutor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASkillExecutor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

