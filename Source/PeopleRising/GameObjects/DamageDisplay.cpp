// Fill out your copyright notice in the Description page of Project Settings.

#include "DamageDisplay.h"
#include "Runtime/CoreUObject/Public/UObject/ConstructorHelpers.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"


// Sets default values
ADamageDisplay::ADamageDisplay()
{
	text = CreateDefaultSubobject<UTextRenderComponent>("Text");
	RootComponent = text;
	text->bVisible = false;
	text->WorldSize = 70;
	text->TextRenderColor = FColor::Emerald;
	//ParticleSystem'/Game/StarterContent/Particles/P_Sparks.P_Sparks'
     ConstructorHelpers::FObjectFinder<UCurveFloat> Curve(TEXT("/Game/Data/Curves/DamageDisplayCurveUp"));
	check(Curve.Succeeded());

	FloatCurve = Curve.Object;
	 ConstructorHelpers::FObjectFinder<UParticleSystem> PS(TEXT("ParticleSystem'/Game/StarterContent/Particles/P_Sparks.P_Sparks'"));
	EmitterTemplate = PS.Object;

}

// Called when the game starts or when spawned
void ADamageDisplay::BeginPlay()
{
	FOnTimelineFloat onTimelineCallback;
	FOnTimelineEventStatic onTimelineFinishedCallback;
	Super::BeginPlay();
	if (FloatCurve != NULL)
	{
		MyTimeline = NewObject<UTimelineComponent>(this, FName("TimelineAnimation"));
		MyTimeline->CreationMethod = EComponentCreationMethod::UserConstructionScript; // Indicate it comes from a blueprint so it gets cleared when we rerun construction scripts
		this->BlueprintCreatedComponents.Add(MyTimeline); // Add to array so it gets saved
		MyTimeline->SetNetAddressable();	// This component has a stable name that can be referenced for replication

		MyTimeline->SetPropertySetObject(this); // Set which object the timeline should drive properties on
		MyTimeline->SetDirectionPropertyName(FName("TimelineDirection"));

		MyTimeline->SetLooping(false);
		MyTimeline->SetTimelineLength(5.0f);
		MyTimeline->SetTimelineLengthMode(ETimelineLengthMode::TL_LastKeyFrame);

		MyTimeline->SetPlaybackPosition(0.0f, false);

		//Add the float curve to the timeline and connect it to your timelines's interpolation function
		onTimelineCallback.BindUFunction(this, FName{ TEXT("TimelineCallback") });
		onTimelineFinishedCallback.BindUFunction(this, FName{ TEXT("TimelineFinishedCallback") });
		MyTimeline->AddInterpFloat(FloatCurve, onTimelineCallback);
		MyTimeline->SetTimelineFinishedFunc(onTimelineFinishedCallback);

		MyTimeline->RegisterComponent();
	}
	
}

void ADamageDisplay::TimelineCallback(float val)
{
	text->AddLocalOffset(FVector(0, 0, val * 10));
}

void ADamageDisplay::TimelineFinishedCallback()
{
	Destroy();
}

void ADamageDisplay::ShowDamageDisplay(int32 damageAmount)
{
	text->bVisible = true;
	text->Text = FText::AsNumber(damageAmount);
	UGameplayStatics::SpawnEmitterAttached(EmitterTemplate, text);
	if (MyTimeline != NULL)
	{
		MyTimeline->PlayFromStart();
	}
}

void ADamageDisplay::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (MyTimeline != NULL)
	{
		MyTimeline->TickComponent(DeltaTime, ELevelTick::LEVELTICK_TimeOnly, NULL);
	}
}





