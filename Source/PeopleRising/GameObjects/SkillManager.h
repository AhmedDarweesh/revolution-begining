// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "UObject/NoExportTypes.h"
#include "StructsManager.h"
#include "WarriorManager.h"
#include "WarriorBase.h"
#include "SkillManager.generated.h"


/**
 * 
 */
UCLASS(Blueprintable)
class PEOPLERISING_API USkillManager : public UObject
{
	GENERATED_BODY()
public:
	USkillManager(UWarriorManager*Wmanager);
	USkillManager();
	UFUNCTION()
		FSkillData GetMorphSkill(const FSkillData OriginalSkill);
	UFUNCTION()
		void SetWarriorManager(UWarriorManager*Wmanager);
	UFUNCTION()
		bool ShoulSkillMorph(const FSkillData OriginalSkill,AWarriorBase* Initiator, TArray<AWarriorBase*> Targets);
	
private:
	
	UPROPERTY()
	UWarriorManager*Wmanager;
};
