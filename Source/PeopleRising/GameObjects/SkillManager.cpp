// Fill out your copyright notice in the Description page of Project Settings.
#include "GameObjects/SkillManager.h"

USkillManager::USkillManager(UWarriorManager*Wmanager)
{
	UE_LOG(LogTemp, Warning, TEXT("parameter construtor"));
	this->Wmanager = Wmanager;
}

USkillManager::USkillManager()
{
	UE_LOG(LogTemp, Warning, TEXT("no parameter construtor"));
}

FSkillData USkillManager::GetMorphSkill(const FSkillData OriginalSkill)
{
	return Wmanager->loadSkill(OriginalSkill.MorphSkillPointer);
}

void USkillManager::SetWarriorManager(UWarriorManager * Wmanager)
{
	UE_LOG(LogTemp, Warning, TEXT("wmanager set"));
	this->Wmanager = Wmanager;
}

bool USkillManager::ShoulSkillMorph(const FSkillData OriginalSkill, AWarriorBase * Initiator, TArray<AWarriorBase*> Targets)
{
	if (!OriginalSkill.CanMorph)
	{
		UE_LOG(LogTemp, Warning, TEXT("skill can't morph"));
		return false;
	}
	if (!Wmanager)
	{
		UE_LOG(LogTemp, Warning, TEXT("wmanger is null"));
		return false;
	}
	FSkillMorphInfo MorphData = OriginalSkill.MorphData;

	switch (MorphData.MorphType)
	{
	case FSkillMorphType::HPLower:
		return Initiator->IsHPLower(MorphData.HPRange);

	case FSkillMorphType::HPHigher:
		return Initiator->IsHPHigher(MorphData.HPRange);
		
	case FSkillMorphType::HasBuff:
		return Initiator->HasBuffOfType(MorphData.BuffStatus);
	default:
		break;
	}
	return false;
}
