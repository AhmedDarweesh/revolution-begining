// Fill out your copyright notice in the Description page of Project Settings.

#include "WarriorSpawner.h"


// Sets default values
AWarriorSpawner::AWarriorSpawner()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

AWarriorBase* AWarriorSpawner::SpawnWarrior(FWarriorData WarriorData, FTransform SpawnLocation, FVector2D Cooridinates, bool IsCpu, TSubclassOf<AWarriorBase>WarriorClass, ABattleController* GameController, TArray<FSoftObjectPath>ObjectsToLoad)
{
	AWarriorBase* WarriorToSpawn = GetWorld()->SpawnActorDeferred<AWarriorBase>(WarriorClass, SpawnLocation);
	WarriorToSpawn->CurrentLocation = Cooridinates;

	return WarriorToSpawn;
}

// Called when the game starts or when spawned
void AWarriorSpawner::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AWarriorSpawner::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AWarriorSpawner::AddWarriorProperties(AWarriorBase* Warrior, FWarriorData warriordata, FClassData ClassData, FTransform WarriorTransform, ABattleController* GameController, TArray<FSoftObjectPath>ObjectsToLoad)
{
	
}


