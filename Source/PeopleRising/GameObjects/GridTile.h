// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GridTile.generated.h"
class AWarriorBase;
UCLASS()
class PEOPLERISING_API AGridTile : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AGridTile();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Game")
	FVector2D Cordinates;
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Battle Actions")
	bool CanMoveTo(FVector2D CurrentLocation, FVector2D TargetLocation, int32 MovementRange);
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Battle Actions")
	void HighLightTile();
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Battle Actions")
		void UnHighLightTile();
	bool IsOccupied;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Game")
		AWarriorBase*CurrentWarrior;
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Game")
		FVector GetCenterLocation();

	TArray<FVector2D> GetAvailableMoves(FVector2D CurrentLocation, TArray<FVector2D> Directions, int32 MovementRange);
};
