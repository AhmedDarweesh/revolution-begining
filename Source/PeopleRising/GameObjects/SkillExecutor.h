// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "WarriorBase.h"
#include "Startegies/Interfaces/EffectApplyerStartegy.h"
#include "StructsManager.h"
#include "SkillExecutor.generated.h"

UCLASS()
class PEOPLERISING_API ASkillExecutor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASkillExecutor();
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Battle Actions")
		void ExecuteSkill(FSkillData Skill, AWarriorBase * InitiatorWarrior,const TArray<AWarriorBase *>& Targets, UObject* Effector);
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Battle Actions")
		void ApplyEffect();
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Battle Actions")
		void ShowVFX();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(BlueprintReadWrite)
		TArray<AWarriorBase *>SkillTargets;
	UPROPERTY(BlueprintReadWrite)
	FSkillData CurrentSkill;
	UPROPERTY(BlueprintReadWrite)
		AWarriorBase * Initiator;
	UObject* EffectApplyer;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	
	
};
