// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/TimelineComponent.h"
#include "Components/TextRenderComponent.h"
#include "Particles/ParticleSystemComponent.h"
#include "GameFramework/Actor.h"
#include "DamageDisplay.generated.h"

UCLASS()
class PEOPLERISING_API ADamageDisplay : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ADamageDisplay();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	UPROPERTY()
		UTimelineComponent* MyTimeline;
	UPROPERTY()
		UTextRenderComponent*text;

	UPROPERTY()
		UCurveFloat* FloatCurve;
	UPROPERTY()
		UParticleSystem* EmitterTemplate;

	UFUNCTION()
		void TimelineCallback(float val);

	UFUNCTION()
		void TimelineFinishedCallback();

	

	UPROPERTY()
		TEnumAsByte<ETimelineDirection::Type> TimelineDirection;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	void ShowDamageDisplay(int32 damageAmount);

	
	
};
