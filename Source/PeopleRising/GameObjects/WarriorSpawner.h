// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BattleController.h"
#include "StructsManager.h"
#include "GameFramework/Actor.h"
#include "WarriorSpawner.generated.h"

UCLASS()
class PEOPLERISING_API AWarriorSpawner : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWarriorSpawner();
	UFUNCTION(BlueprintCallable,Category="Warrior spawn")
	AWarriorBase* SpawnWarrior(FWarriorData WarriorData, FTransform SpawnLocation, FVector2D Cooridinate, bool IsCpu, TSubclassOf<AWarriorBase>WarriorClass,ABattleController* GameController,TArray<FSoftObjectPath>ObjectsToLoad);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
private:
	UFUNCTION()
		void AddWarriorProperties(AWarriorBase* Warrior, FWarriorData warriordata, FClassData ClassData, FTransform WarriorTransform, ABattleController* GameController, TArray<FSoftObjectPath>ObjectsToLoad);

	
	
};
