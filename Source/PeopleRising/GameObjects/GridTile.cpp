// Fill out your copyright notice in the Description page of Project Settings.

#include "GridTile.h"


// Sets default values
AGridTile::AGridTile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

}

// Called when the game starts or when spawned
void AGridTile::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AGridTile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AGridTile::HighLightTile_Implementation()
{
}

FVector AGridTile::GetCenterLocation_Implementation()
{
	return FVector();
}

void AGridTile::UnHighLightTile_Implementation()
{
}

TArray<FVector2D> AGridTile::GetAvailableMoves(FVector2D CurrentLocation, TArray<FVector2D> Directions, int32 MovementRange)
{
	TArray<FVector2D> Moves ;
	for (auto Direction : Directions)
	{
		int32 MoveMagnitude = MovementRange;
		int xstart = FMath::Abs(Direction.X);
		int ystart = FMath::Abs(Direction.Y);
		int32 maxXMovement = MovementRange - ystart;
		int32 maxYMovement = MovementRange - xstart;
		for (int32 i = xstart; i <= maxXMovement; i++)
		{
			for (int32 j = ystart; j <= maxYMovement; j++)
			{
				if (i + j > MoveMagnitude)
				{
					continue;
				}
				int newX = Direction.X *i;

				int newY = Direction.Y * j;
				FVector2D possibleLocation(newX + CurrentLocation.X, newY + CurrentLocation.Y);
				if (possibleLocation.Y<0 || possibleLocation.X<0) {
					continue;
				}
				Moves.AddUnique(possibleLocation);
			}
		}
	
	
	}
	return Moves;
}
//TArray<FVector2D> AGridTile::GetAvailableMoves(FVector2D CurrentLocation, TArray<FVector2D> Directions, int32 MovementRange)
//{
//	TArray<FVector2D> Moves;
//	for (int i = MovementRange*-1; i <= MovementRange; i++) {
//		int move = MovementRange - FMath::Abs(i);
//
//		FVector2D move1 (CurrentLocation.X + i, CurrentLocation.Y + move);
//		FVector2D move2 (CurrentLocation.X + move, CurrentLocation.Y + i);
//		FVector2D move3 (CurrentLocation.X + i, CurrentLocation.Y);
//		FVector2D move4 (CurrentLocation.X, CurrentLocation.Y + i);
//		Moves.AddUnique(move1);
//		Moves.AddUnique(move2);
//		Moves.AddUnique(move3);
//		Moves.AddUnique(move4);
//	}
//	return Moves;
//}

bool AGridTile::CanMoveTo_Implementation(FVector2D CurrentLocation, FVector2D TargetLocation, int32 MovementRange)
{
	return (CurrentLocation-TargetLocation).Size()<MovementRange;
}

