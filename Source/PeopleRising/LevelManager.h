// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "UObject/NoExportTypes.h"
#include "StructsManager.h"
#include "WarriorManager.h"
#include"Runtime/LevelSequence/Public/LevelSequencePlayer.h"
#include "LevelManager.generated.h"


/**
 * 
 */
UCLASS(Blueprintable)
class PEOPLERISING_API ULevelManager : public UObject
{
	GENERATED_BODY()
public:
	ULevelManager();
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable,Category=Level)
		FLevelData  loadLevelData(const FString&  levelName);
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = Level)
		FEnemiesFormation  loadLevelEnemies(const FLevelData&  levelName);
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Events)
			UDataTable * LevelList;
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Events)
			UDataTable * EventsList;
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Events)
			UDataTable * EnemiesFormations;
		UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = Warriors)
			TArray<FWarriorData>loadWarriors(const TArray<FString>&  WarriorsNames);
		UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = Warriors)
			TArray<FWarriorData>loadDefaultWarriors();
		UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = Events)
		FEventData GetFirstAvailableEvent(const FLevelData & row, const TArray<FString>&  FinishedEvenets) ;
		UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = Warriors)
		FClassData loadClassData(const FString& ClassId);
		UFUNCTION( BlueprintCallable, Category = Warriors)
			TMap<FString,FClassData> loadAvailableClasses(const FWarriorData Warrior);
		UFUNCTION(BlueprintCallable, Category = Warriors)
			TMap<FString, FSkillData> loadClassSkills(const TArray<FString>SkillIds);
		UFUNCTION(BlueprintCallable, Category = Warriors)
			FWarriorData LearnSkill(const FWarriorData Input,const FString NewSkill);
		UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = Warriors)
			FWarriorData ScaleWarriorStatus(const FClassData& Class, const FWarriorData& warrior);
private:
	UFUNCTION()
	bool HasAllRequiredEventsDone(const TArray<FString>&  FinishedEvenets, const TArray<FString>&  RequiredEvents);
	UPROPERTY()
	UWarriorManager*Wmanager;
};
