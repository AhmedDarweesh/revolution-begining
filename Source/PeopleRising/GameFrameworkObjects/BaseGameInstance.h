// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "StructsManager.h"
#include "WarriorManager.h"
#include "GameObjects/SkillManager.h"
#include "Engine/GameInstance.h"
#include "MoviePlayer.h"
#include "BaseGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class PEOPLERISING_API UBaseGameInstance : public UGameInstance
{
	GENERATED_BODY()
	
public:

	UFUNCTION(BlueprintCallable)
	virtual void Init() override;
	UFUNCTION(BlueprintCallable)
	 TMap<FVector2D, FWarriorData> GetWarriorsMap(TArray<int32>Positions,TArray<FWarriorData>Warriors,int32 Rows) ;
	UFUNCTION()
		virtual void BeginLoadingScreen(const FString& MapName);
	UFUNCTION()
		virtual void EndLoadingScreen(UWorld* InLoadedWorld);
	UFUNCTION(BlueprintCallable)
		 UWarriorManager* GetWarriorsManager();
	UFUNCTION(BlueprintCallable)
		USkillManager* GetSkillManager();

private:
	UPROPERTY()
	 UWarriorManager* WarriorManager;
	UPROPERTY()
		USkillManager* SkillManager;
	
};
