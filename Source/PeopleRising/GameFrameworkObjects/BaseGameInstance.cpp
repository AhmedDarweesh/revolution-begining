// Fill out your copyright notice in the Description page of Project Settings.

#include "BaseGameInstance.h"

void UBaseGameInstance::Init()
{
	Super::Init();
	UE_LOG(LogTemp, Warning, TEXT("Init method called"));
	FCoreUObjectDelegates::PreLoadMap.AddUObject(this, &UBaseGameInstance::BeginLoadingScreen);
	FCoreUObjectDelegates::PostLoadMapWithWorld.AddUObject(this, &UBaseGameInstance::EndLoadingScreen);
	WarriorManager = NewObject<UWarriorManager>();
	SkillManager = NewObject<USkillManager>();
	SkillManager->SetWarriorManager(WarriorManager);
}
TMap<FVector2D, FWarriorData> UBaseGameInstance::GetWarriorsMap(TArray<int32>Positions, TArray<FWarriorData>Warriors, int32 Rows)
{
	TMap<FVector2D, FWarriorData>WarriorsMap;
	for (int i = 0; i < Positions.Num(); i++)
	{
		int32 Position = Positions[i];
		int32 X = Position%Rows;
		int32 Y = Position / Rows;
		FVector2D GridLocation(X, Y);
		WarriorsMap.Add(GridLocation, Warriors[i]);

	}
	return WarriorsMap;
}
void UBaseGameInstance::BeginLoadingScreen(const FString & MapName)
{
	if (!IsRunningDedicatedServer())
	{
		FLoadingScreenAttributes LoadingScreen;
		LoadingScreen.bAutoCompleteWhenLoadingCompletes = false;
		
		LoadingScreen.WidgetLoadingScreen = FLoadingScreenAttributes::NewTestLoadingScreenWidget();

		GetMoviePlayer()->SetupLoadingScreen(LoadingScreen);
	}
}

void UBaseGameInstance::EndLoadingScreen(UWorld * InLoadedWorld)
{
}

UWarriorManager* UBaseGameInstance::GetWarriorsManager()
{
	if (WarriorManager == nullptr)
	{
		WarriorManager = NewObject<UWarriorManager>();
	}
	return WarriorManager;
}

USkillManager * UBaseGameInstance::GetSkillManager()
{
	if (SkillManager == nullptr)
	{
		SkillManager = NewObject<USkillManager>(WarriorManager);
	}
	return SkillManager;
}
